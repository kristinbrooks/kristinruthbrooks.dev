# kristinruthbrooks.dev

This repo contains records of many of the things I've done since the beginning of my journey to become a professional 
self-taught programmer. To start I will put it up on an extremely basic site, but as I learn more and more the site 
will be upgraded until it is both beautiful and functional.
