/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    screens: {
      'tablet': '640px',
      // => @media (min-width: 640px) { ... }

      'laptop': '1024px',
      // => @media (min-width: 1024px) { ... }

      'desktop': '1280px',
      // => @media (min-width: 1280px) { ... }
    },
    extend: {
      backgroundImage: {
        'hero-mobile': "url('/hero-mobile.jpg')",
        'hero-tablet-profile': "url('/hero-tablet-profile.jpg')",
        'hero-tablet-landscape': "url('/hero-tablet-landscape.jpg')",
        'hero-laptop': "url('/hero-laptop.jpg')",
        'hero-desktop': "url('/hero-desktop.jpg')",
      }
    },
  },
  plugins: [require('@tailwindcss/typography'), require('daisyui')],
  // https://daisyui.com/docs/config/
  daisyui: {
    styled: true,
    themes: ['autumn', 'coffee'],
    base: true,
    utils: true,
    logs: true,
    rtl: false,
    prefix: '',
    // darkTheme: 'coffee',
  },
}
