**NOTE**: These notes are based on Section 5 in [Build Responsive Real World Websites with HTML5 and CSS3](https://www.udemy.com/course/design-and-develop-a-killer-website-with-html5-and-css3/) 
by Jonas Schmedtmann --- so they are his design opinions :) 

# Responsive Web Design

1. **Fluid grid**: all layout elements are sized in relative units, such as percentages, instead of absolute units
 like pixels
 
2. **Flexible images**: are also sized in relative units

3. **Media queries**: allow us to specify different CSS style rules for different browser widths
