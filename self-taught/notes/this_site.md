# Resources used to build site 

* https://kit.svelte.dev/docs/introduction
* https://pnpm.io/installation
* https://github.com/sveltejs/kit/discussions/4857
* https://kit.svelte.dev/docs/routing
* https://kit.svelte.dev/docs/routing#page-page-js
* https://kit.svelte.dev/docs/types#generated-types
* https://github.com/sveltejs/kit/issues/2712
* https://kit.svelte.dev/docs/advanced-routing#rest-parameters
* https://www.npmjs.com/package/svelte-markdown
* https://stackoverflow.com/questions/72483848/how-can-i-get-all-files-in-a-directory-with-svelte/72859846#72859846
* https://vitejs.dev/guide/features.html#glob-import
* https://svelte.dev/tutorial/reactive-declarations
* https://docs.netlify.com/integrations/frameworks/sveltekit/
* https://github.com/sveltejs/kit/issues/6462
* https://tailwindcss.com/docs/guides/sveltekit
* https://daisyui.com/docs/install/
