# Haskell Tuples

* use `()` 
* used to store several heterogeneous elements (elements of different types) as a single value
* tuples have a fixed size, so you must know how many elements you'll be storing ahead of time
    * the reason tuples are so rigid in this way is because the size of a tuple is part of its type 
* tuples of the same length but different data types are considered distinct types of tuples
* can be compared if their components can be compared
    * can't compare tuples of different sizes
* the number of values in a tuple is known as its *arity*
    * no such thing as a singleton tuple --- will give an error
    * most will be 5-tuple or smaller

## 2-tuple or Pair

* has 2 elements
* datatype declaration: 
  ```
  Prelude> :info (,)
  data (,) a b = (,) a b
  ```
  * this is a product type (not a sum type) --- it represents logical conjunction: you must
   supply both arguments to construct a value
  * the type variables are different, which allows for tuples that contain values of different
   types (but they don't have to be different)
* can use their syntax to pattern match

## Functions for Working with Pairs

* `fst tuplePair` - returns the first component (`fst :: (a, b) -> a`)
* `snd tuplePair` - returns the second component (`snd :: (a, b) -> b`)
* `swap tuplePair` can be imported from `Data.Tuple` - swaps the two values in the 2-tuple 
