# Syntax in Haskell Functions

## Pattern Matching

* when defining functions in Haskell you can create separate function bodies
 for different patterns
* can pattern match on almost any data type
* the last pattern should be a catchall pattern which will match anything 
* example:
  ```
  sayMe :: Int -> String
  sayMe 1 = "One!"
  sayMe 2 = "Two!"
  sayMe 3 = "Three!"
  sayMe 4 = "Four!"
  sayMe 5 = "Five!"
  sayMe x = "Not between 1 and 5"
  ```
    * writing it this way saves us from writing a complicated if/then/else
     statement
    * the `x` pattern has to go last or it will stop there and not check any
     following patterns -- this is the catchall pattern
* if don't account for all inputs will get a 'Non-exhaustive patterns in
 functions' exception => we always need a catchall pattern at the end

### Pattern Matching with Tuples

* example without pattern matching:
  ```
  addVectors :: (Double, Double) -> (Double, Double) -> (Double, Double)
  addVectors a b = (fst a + fst b, snd a + snd b) 
  ```
* example with pattern matching:
  ```
  addVectors :: (Double, Double) -> (Double, Double) -> (Double, Double)
  addVectors (x1, y1) (x2, y2) = (x1 + x2, y1 + y2) 
  ```
    * makes it clear the parameters are tuples
    * already a catchall because we are guaranteed to get two pairs as
     arguments
* example to extract components of triples;
  ```
  first :: (a, b, c) -> a
  first (x, _, _) = x
  
  second :: (a, b, c) -> b
  second (_, y, _) = y
  
  third :: (a, b, c) -> c
  third (_, _, z) = z
  ```
    * can use `_` as a generic placeholder variable when we don't care what
     that part is
     
### Pattern Matching with Lists and List Comprehensions

* example with list comprehension:
  ```
  ghci> let xs = [(1,3), (4,3), (2,4), (5,3), (5,6), (3,1)]
  ghci> [a + b | (a, b) <- xs]
  [4,7,6,8,11,4]
  ```
* if the pattern match fails, the list comprehension will just move to the
 next element and the failed element won't be in the resulting list
* with regular lists you can match the empty list `[]` or any pattern that
 involves `:` and the empty list (remember that `[1,2,3]` is the same as `1:2
 :3:[]`)
    * patterns that include `:` will only match against lists of length 1 or
     more
* example: the pattern `x:xs` will bind the head of the list to `x` and the
 rest to `xs` --- if the list only has a single element then `xs` will be the
  empty list
    * this pattern is used often in recursive functions
* example of our own `head` function with pattern matching:
  ```
  head' :: [a] -> a
  head' [] = error "Can't call head on the empty list, dummy!"
  head' (x:_) = x
  ```
    * to bind something to several variables we must surround them in
     parenthesis or Haskell can't properly parse them
     * don't use functions that intentionally cause runtime errors too much
     , but in this case it just doesn't make sense to call `head'` on the
      empty list so we did it
  ```
  ghci> head' [4,5,6]
  4
  ghci> head' "Hello"
  H
  ```
* example: 
  ```
  badAdd :: (Num a) => [a] -> a
  badAdd (x:y:z:[]) = x + y + z
  ```
  ```
  ghci> badAdd [100, 20]
  *** Exception: Non-exhaustive patterns in function badAdd
  ```
    * we didn't handle cases of lists that don't have three elements, but the
     type declaration says it can take any `Num` type list --- need a catchall
* can't use `++` in pattern matching

### As-patterns

* allow you to break up an item according to a pattern, while still keeping a
 reference to the original item
* to create an `as-pattern` precede a regular pattern with a name and an `@`
* example:
  ```
  firstLetter :: String -> String
  firstLetter [] = "Empty string, whoops!"
  firstLetter all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]
  ``` 
  ```
  ghci> firstLetter "Dracula"
  "The first letter of Dracula is D"
  ```
    * could still reference the original string `all`, but got `[x]` from the
     pattern match
     
## Guards
