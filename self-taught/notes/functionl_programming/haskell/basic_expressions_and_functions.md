# Basic Expressions and Functions

## Expressions

* Expressions are the most basic units of Haskell program --- a function is a
 type of expression --- an expression that that is applied to an argument and
  always returns a result
* they are related to functions in mathematics --- they map an input to an
 output
* always evaluate to the same result when given the same values because  they
 are purely built
* all functions take one argument and return one result
    * when it seems like we are passing multiple arguments, we are actually
     applying a series of nested functions, each to one argument --- called
      currying
* they allow us to abstract parts of code we want to reuse for different
 literal values
* they are how we factor out a pattern into something we can reuse with
 different inputs
    * done by naming the function and introducing an independent variable
* functions can appear in the bodies of other functions or be used as arguments
* reducible expressions are also called *redexes*

## Defining Functions:

* functions start with their name --- white space --- followed by the formal
 parameters --- separated by white space --- equals sign --- body of function
    * `functionName param1 param2 ... paramN = bodyOfFunction`
    * function name is the *declaration*
    * the parameters correspond to the head of a lambda and bind variables
     that appear in the body
    * `=` is used to define (or declare) values and functions --- it is not
     how we test for equality between two values
     * the body --- an expression that could be evaluated if the function is
      applied to a value
      
## Evaluation

* means reducing the terms until the expression is irreducible --- usually
 called a value
* Haskell uses non-strict evaluation --- called "lazy evaluation" --- defers
 evaluation until they're forced to evaluate by other terms that refer to them
* values are irreducible expressions --- they are a terminal point of reduction
* applications of functions to arguments are reducible
* *normal form* is when there are no remaining reducible expressions
    * Haskell doesn't default to normal form --- it defaults to weak head
     normal form(WHNF)
        * means not everything will get reduced to its irreducible form
         immediately --- evaluation is at the last possible moment
         

      
     

## Associativity and Precedence

* like in math, there is a default associativity and precedence to infix
 operators ---  intuitions from math classes will generally hold in Haskell
    * *associativity* means the grouping of numbers undergoing the operation
     does not change the result
    * *precedence* is the priority of order
* using the `:info` or `:i` command about an operator or function in GHCi will tell
 you its type and whether it's an infix operator --- if it is, it will also
  tell you its associativity and precedence info
    * example:
      ```
      Prelude> :info (*)
      infixl 7 *
      
      Prelude> :info (+) (-)
      infixl 6 +
      
      infixl 6 -
      
      Prelude> :info (^)
      infixr 8 ^
      ``` 
    * GHCi `:info` command format (white space between each)
        * `infixl`/`infixr` means its an infix operator --- the `l` means it's
         left associative, `r` is right associative 
        * 7 is the precedence --- higher is applied first --- scale is 0-9
        * infix function name
* multiplication is associative so shifting the parenthesis around in an
 expression doesn't change the result
    * example: 
      ```
      Prelude> 2 * 3 * 4
      24 
      Prelude> (2 * 3) * 4
      24 
      Prelude> 2 * (3 * 4)
      24 
      ```
* exponentiation is not associative so the parenthesis make a difference
    * example:
      ```
      Prelude> 2 ^ 3 ^ 4
      2417851639229258349412352
      Prelude> 2 ^ (3 ^ 4)
      2417851639229258349412352
      Prelude> (2 ^ 3) ^ 4
      4096
      ```
      
## Declaring Values

* order of declarations in source code file doesn't matter because GCHi loads
 the whole file at once
    * in REPL it does matter
* we declare the *module* name so it can be imported by its name in a project
    * module names are capitalized
* indentation of code is important ---  varying it can potentially change its
 meaning or break your code
    * it often replaces syntactic markers like curly braces, semicolons, and
     parenthesis
    * general rule: code that is part of an expression should be indented
     under the beginning of that expression --- even if that expression is
      not at the leftmost margin
            * parts of the expression that are grouped should be indented at
             the same level
* white space is significant
    * efficient use makes the syntax more concise
    * it is often the only mark of a function call unless parenthesis are
     necessary for precedence
    * trailing white space is considered bad style
* if you get an error message:
    * first line tells where it occurred `:num:num` means line 7, column 1     
* all declarations in a module must start in the same column

## Infix operators

* [Operator List](https://imada.sdu.dk/~rolf/Edu/DM22/F06/haskell-operatorer.pdf) 
* syntax where the function being applied is in the middle of the expression
* generally, the default function syntax is *prefix* --- function being applied
 is at the
 beginning of the expression
* arithmetic operators are functions that default to infix style
    * all operators are functions, not all functions are operators
* can sometimes use functions infix style by using backticks `` around the
 function name
    * example: 
      ```
      Prelude> 10 `div` 4
      2
      ```
      instead of 
      ```
      Prelude> div 10 4
      2
      ```
* can use infix operators in prefix style by putting them in parentheses
    * examples:
      ```
      Prelude> (+) 100 100
      200
      Prelude> (*) 768395 21356345
      16410108716275
      Prelude> (/) 123123 123
      1001.0
      ```

### Comparison Operators

* operators are `==`, `/=`, `>`, `>=`, `<`, `<=`
* can compare things that can be compared and determined to be equal in value (`Eq` type class
) and things that can be ordered (`Ord` type class)
* we can comparison functions with `not` because it evaluates to a `Bool` value


### Arithmetic Operators

* `+` - addition
* `-` - subtraction
* `*` - multiplication
* `/` - fractional division
* `div` - integral division, round down
* `mod` - modulo (like `rem` but after modular division)
* `quot` - integral division, round toward zero
* `rem` - remainder after division

#### `mod` vs `rem`

* `mod` - system of arithmetic for integers where numbers "wrap around" upon
     reaching a certain value --- the modulus
    * example: think about it like a clock --- the arithmetic modulo is 12
     => 12 is
     both 12 and 0
    * if one or both arguments are negative, the results will have the same
     sign as the divisor (dividend / *divisor* = quotient)
* `rem` - if one or both arguments are negative, the results will have the
 same sign as the dividend (*dividend* / divisor = quotient)
* ```
  Prelude> (-5) `mod` 2
  1
  Prelude> 5 `mod` (-2)
  -1
  Prelude> (-5) `mod` (-2)
  -1
  
  
  Prelude (-5) `rem` 2
  -1
  Prelude 5 `rem` (-2)
  1
  Prelude (-5) `rem` (-2)
  -1
  ```
  
## Negative Numbers

* negative numbers get special treatment in Haskell (due to the interaction
 of parenthesis, currying, and infix syntax)
* `Prelude> -1000` is fine because the negative number is by itself, but
 `Prelude> 1000 + -9` is not --- it causes a parsing error because there
 are 2 infix operators of the same precedence in a row --- it needs to be
  `Prelude> 1000 + (-9)`
* parenthesis around a negative indicates to the REPL or compiler that `-` is
 being used as an alias for `negate` 
    * called *syntactic sugar* - means it is making it easier for us to read
* `Prelude> 2000 + (-1234)` is semantically identical (has the same meaning
, despite different syntax) to `Prelude> 2000 + (negate 1234)`, while
 `Prelude> 2000 - 1234` the `-` here means subtraction
* syntactic overloading like this isn't common in Haskell

## Parenthesization

### `$` Operator

* definition of `$` - `f $ a = f a`
    * infix operator with precedence 0, right associative
* used for convenience when you want to express something with fewer pairs
 of parenthesis
* ```
  Prelude> (2^) (2 + 2)
  16
  -- can replace those parenthsis
  Prelude> (2^) $ 2 + 2
  16
  -- without either parenthesis or $
  Prelude> (2^) 2 + 2
  6
  ```
* `$` evaluates everything to its right first --- this can be used to delay
 function application
* can stack up multiple uses of `$`
    * example:
      ```
      Prelude> (2^) $ (+2) $ 3*2
      256
      ```
      works, while
      ```
      Prelude> (2^) $ 2 + 2 $ (*30)
      ```
      does not
      
### Parenthesising Infix Operators

* sometimes want to refer to an infix function without applying any arguments
* also times where you want to use them as prefixes
    * to refer to `>>` as a value you write it as `(>>)`
    * `(+)` is addition infix with no arguments applied yet
    * `(+1)` is addition operator with one argument applied
    * examples:
      ```
      Prelude> 1 + 2
      3
      Prelude> (+) 1 2
      3
      Prelude (+1) 2
      3
      ```
        * that last case is known as sectioning --- it allows you to pass
         around partially applied functions
            * if the function is commutative, like addition, it doesn't
             matter if you use `(+1)` or `(1+)`
            * if the function is not commutative, the order matters
            * example:
              ```
              Prelude> (1/) 2
              0.5
              Prelude> (/1) 2
              2.0
              ```
            * subtraction is a **special** case where sectioning only works one
             way --- because if you write something like `(-x)` the `-` means
             negation not subtraction, you can use `(subtract x)` instead
              --- but you can use it as the first argument like`(x-)`
     