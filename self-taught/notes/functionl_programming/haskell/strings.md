# Haskell Strings

(note: example functions in chapter 3 section of 'haskell-programming-from
-first-principles' repo)

* type `Char` is a character
* `Char` includes alphabetic characters, Unicode characters, symbols, etc.
* a `String` is a list of characters
* `String` is syntactic sugar for `[Char]`
    * it is a type alias (or type synonym) for a list of `Char`
* a string needs to be in `""` (while a character is in `''`)
    
## Printing Simple Strings

* `print` tells GHCi to print the string to the display
    * string will still have quotation marks
    * `print` ca be used for other data types as well
* `putStr` and `putStrLn` are specific to `String`
    * they print the string without quotation marks
    * they have a different type than `print` does
    * the `Ln` in `putStrLn` indicates that it starts a new line
    
## String Concatenation

* concatenate means to link together

### `++` Function

* infix operator
* has the type `[a] -> [a] -> [a]`
    * `a` represents a `Char`
    * `a` is a type variable and is polymorphic
    * every list must be a list of the same type of values --- the literal
     values in each list don't need to be the same, they only need to be the
      same `type`

### `concat` Function

* normal (not infix) function
* has the type `[[a]] -> [a]` (if using newer GHC it will show `Foldable t
 => t [a]` --- for our purposes here `Foldable t` can be thought of as a list)
    * takes a list of lists and flattens it into one list
    * `a` represents the same things as in the above description for `++`
