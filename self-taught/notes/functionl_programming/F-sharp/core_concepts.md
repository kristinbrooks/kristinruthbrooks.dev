# Core Concepts

## What is Functional Programming?

* "Functional Programming is a style of programming that emphasizes the evaluation of expressions, rather than the execution of commands. The expressions in these languages are formed by using functions to combine basic values." [Hutton ed. 2002]
    * "evaluation of expression" represents the functional approach, while "execution of commands" (commands = statements) is imperative style 
        * **execution of statements**: Typically work with objects that can be changed, and the code describes what modifications we need to perform in order to achieve the desired result
        * **evaluation of expressions**: The program code is an expression that specifies properties of the object we want to get as a result. We don't specify the steps necessary to construct the object and we can't accidentally use the object before it's created.
* write code as a series of expressions instead of a sequence of statements

## Imperative(how) vs Declarative(what)

* Example: two ways to describe getting the same information
    * version 1: Take the next customer from a list. If the customer lives in the UK, show their details. If there are more customers in the list, go to the beginning.
    * version 2: Show customer details of every customer living in the UK.
* version 1 describes *how* to achieve our goal, while version 2 describes *what* we want to achieve
* this *how* vs *what* is the difference between imperative and declarative -- the *what* is more readable and reflects the aim of the program.
* functional programming is one way to realize the more general idea of declarative programming
* declarative style makes it possible to ignore implementation details
    * write libraries that hide the implementation details
* declarative libraries can be used in a compositional manner
    
## F# Interactive

* the REPL for F#
* allows you to verify and test code immediately while writing it

### Executing Code

* from command line: type or paste in the code, then type `;;` and press Enter
* from Visual Studio: select the code and press Alt+Enter to send it to the interactive window
* from Jetbrains Rider: click the little light bulb in the gutter by the line number and select either 'Send Line to F# Interactive' or 'Send Selection to F# Interactive' depending on where you want to execute a single line or a code block
