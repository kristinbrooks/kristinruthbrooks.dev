# Basics

## Comments

* lines start with `//`
* blocks are enclosed by `*` and `8`
* lines starting with `///` are XMLDoc comments --- they can include XML tags and markup

## `let`

* used to define data, computed values, and functions
* left of `let` binding is often a simple identifier, a pattern, or function names and arguments
* right of `let` binding (after the `=`) is an expression

## Values

* called variables in other languages but since they aren't mutable unless marked `mutable` they are called values instead in F#
* some values are completely immutable, called immutable values

## Type Inference

* code is analyzed to collect constraints from the way you use names --- they must be consistent, which ensures the program is well typed (if it isn't you get a type error)
* automatically generalizes code if it is generic in certain obvious ways

## Lightweight Syntax

* indentation determines where constructs start and finish
* `;;` is used to terminate entries in F# Interactive
* use `in` to write `let` definitions on a single line
  * example: `let pat = expr1 in expr2`
  * the compiler inserts the `in` if `expr2` is column-aligned with the `let` keyword on a subsequent line

## Scope

* for variables defined with `let`, the scope is the entire expression that follows the definition, but not the definition itself
  * they follow a top-down order
    * ```
      let badDefinition1 =
          let words = splitAtSpaces text
          let text = "We three kings"
          words.Length
      ```
      * `error FS0039: The value or constructor 'text' is not defined`     
    * ```
      let badDefinition2 = badDefinition2 + 1
      ```
      * `error FS0039: The value or constructor 'badDefinition2' is not defined` 