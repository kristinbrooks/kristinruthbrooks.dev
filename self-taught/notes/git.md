# Git

* version control system
* [Docs](https://git-scm.com/docs)

Table of Contents
=================

* [General](#general)
* [Branching Strategies](#branching-strategies)
* [Making/Connecting Repositories](#making-/-connecting-repositories)
  * [Workflows](#workflows)
    * [Starting Locally](#starting-locally)
      * [Version 1](#version-1)
      * [Version 2](#version-2)
    * [Starting with a Blank Repository](#starting-with-a-blank-repository)
* [Updating a Branch to Main](#updating-a-branch-to-main)
* [Removing a File that was Mistakenly Committed and Pushed to the Remote]()
* [Replacing Main with Another Branch]()
* [Commands List](#commands-list)
  * [Help](#help)
  * [Status](#status)
  * [Log](#log)
  * [Checkout](#checkout)
  * [Branch](#branch)
  * [Switch](#switch)
  * [Cherry-pick](#cherry-pick)
  * [Pull](#pull)
  * [Fetch](#fetch)
  * [Stash](#stash)
  * [Add](#add)
  * [Commit](#commit)
  * [Rebase](#rebase)
  * [Merge](#merge)
  * [Diff](#diff)
  * [Push](#push)
  * [Remote](#remote)
  * [Clean](#clean)
  * [Reset](#reset)
* [Helpful Links](#helpful-links)

## General

* All git commands should always be run from the project root
* `HEAD` - currently checked out commit sha
* when creating new GitHub repository include a `.gitignore` in the root directory - this allows us to avoid committing 
  unnecessary files that would be committed every time 
  * add `.idea` to stop JetBrains IDE settings from being committed to repo
  * [gitignore.io](https://www.toptal.com/developers/gitignore) - generate .gitignore files for projects
* `cat .git/config` - see config for project - can look at branches, remotes, etc.
* `.gitconfig [options]` - global config, located in user directory
    * new GitHub/GitLab account requires 1 time config setup of email and username
    * can set aliases, which editor to use, etc. here

## Branching Strategies

* [Git Branching Strategies vs. Trunk-Based Development](https://launchdarkly.com/blog/git-branching-strategies-vs-trunk-based-development/)

## Making / Connecting Repositories

### Workflows:

#### Starting Locally

##### Version 1

1. Run `git init` in the root of the project - initializes empty local git repository for a new project
   * `master` is the `git init` default, but you can override this to use `main` instead with
   `git config --global init.defaultBranch main`
   * if you don't have it set to use `main` globally you can rename `master` to `main` on local and remote:
   * ```
     git branch -m master main
     git push -u origin main
     git push origin --delete master
     ```
   * may have to allow the deleting or change the default on GitHub/GitLab to make this work
2. Make a blank repository on GitHub/GitLab with the same name as the directory of your local project (this is the 
convention)
3. Copy either the `HTTPS` or `SSH` address from the modal that pops up when you click the green Code button on 
GitHub or the blue Clone button on GitLab
4. Run `git remote add origin <HTTPS-or-SSH-copied-in-previous-step>` - connects local repository the blank 
repository at the provided address

##### Version 2

NOTE: I think this only works on GitLab

1. Run `git init` in the root of the project (see notes about this command in previous workflow)
2. `git push --set-upstream git@gitlab.com:<your-gitlab-name-here>/<new-project-directory-name-here>.git 
   <main-or-master>`- makes a new project on GitLab and pushes my local repository to main (by default the new 
   repository will be private, can be changed in the settings to be public)

#### Starting with a Blank Repository

1. Make blank repository on GitHub/GitLab
2. Copy either the `HTTPS` or `SSH` address from the modal that pops up when you click the green Code button on 
GitHub or the blue Clone button on GitLab
3. Run `git clone <HTTPS-or-SSH-copied-in-previous-step>` - clones the remote git repository with provided address

## Updating a Branch to Main

NOTES:
1. This example is only for a rebase workflow
2. It's easier to resolve any conflicts that occur if the branch is squashed first


```
git switch main
git pull
git switch <branch_name>
git rebase main
```
If there are conflicts:
* switch from terminal to JetBrains IDE:
    * `cmd-shift-a` -> Resolve Conflicts
    * select file with conflicts and hit enter to open
    * resolve all conflicts, working most to least severe (marked by color of highlight)
        * if *VERY* certain this is what you want, you can accept all changes from the left or right
    * when all conflicts in the file are resolved, click to accept the changes
    * repeat for all files on the list of files with conflicts
* after all conflicts are resolved switch back to the terminal and do:
  ```
  git rebase add . 
  git rebase --continue
  ```
To finish, run:
```
git push --force-w
```

## Removing a File that was Mistakenly Committed and Pushed to the Remote

* `git rm --cached <path-from-root/file-name>` - removes the file from the git repository
* `git rm -r --cached <path-from-root/directory-name>` - recursively removed the whole directory from the git repository

NOTES: 
1. If you don't put the `--cached` in the commands they will **remove the file/directory from the filesystem (i.e. 
   computer)** not just the git repository - this is probably not what you want
2. Don't accidentally `git add` the removed file. After you run one of these commands you should just `git commit` 
   and `git push`
3. Remember to add the removed file/directory to `.gitignore` so it doesn't just end up getting committed again

## Replacing Main with Another Branch

NOTE: only do this if you're *VERY* sure it's what you want to do


* This is using the 'ours' strategy -> (from the docs) "This resolves any number of heads, but the resulting tree of 
  the merge is
always that of the current branch head, effectively ignoring all changes from all other branches. It is meant to be 
used to supersede old development history of side branches. "

```
git checkout <branch>
git merge --strategy=ours main
git checkout main
git merge <branch>
```


## Commands List

NOTE: This is far from an exhaustive list -- these are just the ones I've used and recorded here for personal
reference. See the Docs link above for all possible commands.
 
### Help
 
> * `git command --help` (`-h`) - to see help menu in the terminal

### Status 

> * personal alias -> `gst`
> * `git status` - shows which files need to be staged, are staged, committed, etc

### Log

> * `git log` - shows changes
> * `git log -p` - shows paginated changes
> * `git log -n <int>` - shows <int> number of records
> * `git log --author="user_name"` - search commits by author
>   * can use user_name or regex
> * `git log --graph` - shows paths
> * `git log --name-status` - only shows names of files changed in a commit
>   * will have `A`, `M`, or `D` at the beginning of lines to show if the file was added, modified, or deleted
> * `q` - to quit log
> * `git reflog` - shows history of all the refs(places your head has changed to)
 
### Checkout 

> * personal alias -> `co`
> * `git checkout <branch_name>` - checkout (switch to) the branch
> * `git checkout .` - reverts committed files that have been modified
> * `git checkout -b <branch_name>` - create and checkout a new branch in one step
> * `git checkout <commit_sha>` - checks out the branch with that commit sha
> * `git checkout <branch_name> -- <file_name>` - restore a specific file from a branch if it was deleted locally
>   * `git checkout origin/main -- missing.js` - restores the file missing.js from main
 
### Branch
 
> * `git branch` - see local branches
> * `git branch -r` - see remote branches
> * `git branch -a` - see all local and remote branches
> * `git branch <new_branch_name>` - creates a new branch, but does not switch to it
> * `git branch --delete <branch_name>` (`-d`) - deletes the local branch (only if it has been merged)
> * `git branch --delete --force <branch_name>` (`-D`) - deletes the local branch whether it's been merged or not
 
### Switch 

> * personal alias -> `sw`
> * `git switch <name_of_branch_want_to_switch_to>` - switch branches (old way is `git co branch_name`)
> * `git switch -c <new_branch_name>` - create new branch and switch to it (switch alias `sw`)
 
### Cherry-pick
 
> * `git cherry-pick <commit_sha>` - puts the sha on the tip of a new branch
>   * if doing this make sure remaining commit/s on original branch are still intact
> * `git cherry-pick <current-branch>..<other-branch>` - cherry-pick from another branch to the current branch
>   * example:
>       ```
>       git checkout branch1
>       git cherry-pick branch1..branch2
>       ```
>     * start with this:
>       ```
>       a -- b -- c                    <-- main
>             \    \
>              \    d -- e             <-- branch1
>               \
>                f -- g                <-- branch2
>       ```
>     * get this:
>       ```
>       a -- b -- c                    <-- master
>             \    \
>              \    d -- e -- f -- g   <-- branch1
>               \
>                f -- g                <-- branch2
>       ```

### Pull

> * `git pull` - runs `git fetch` and then `git merge` to merge the retrieved branch heads into the current branch
> * `git pull --rebase` - fetches and then rebases instead of merging
>   * **NOTE**: my global config is set to do this by default without the flag
> * `git pull <remote_name> <branch_name>` - pull changes from a specific branch

### Fetch

> * `git fetch` - gets the updated versions of the remote tracking branches
>   * if no remote is specified the default `origin` remote will be used
> * `git fetch --all` - gets all remote tracking branches for the repo

### Stash

> * allows you to switch branches without committing your current changes
> * `git stash -u` - stashes local unstaged changes
> * `git stash pop` - pops the changes from the stash

### Add
 
> * `git add <file_name>`- stages the file
>   * can list multiple files with spaces between them
> * `git add .` - stages all changed files in the current directory

### Commit 

> * alias -> `ci`
> * `git commit` - commits and opens VIM (or whichever editor specified in config) to add commit message
> * `git commit -m "notes about commit here"` - commits without opening VIM
> * `git commit --amend` - edit/add to last commit, VIM will open to edit commit message
> * `git commit --amend --no-edit` - edit/add to last commit without changing commit message
> * [Writing good commit messages](https://github.com/erlang/otp/wiki/Writing-good-commit-messages)

### Rebase

> * `git rebase` - sets aside your changes, updates your local branch to be up-to-date, then applies your changes on top
>     * if you rebase, must force push afterwards
>     * example of `git rebase main`:
>       * before rebase:
>         ```
>         a -- b -- c -- d              <-- main
>               \
>                e -- f                 <-- branch
>         ```
>       * after rebase:
>         ```
>         a -- b -- c -- d              <-- main
>                         \
>                          e -- f       <-- branch
>         ```
> * `git rebase -i <commit_sha>` - interactive rebase - allows you to edit your commit history
>   * options for each commit: pick, reword, edit, squash, fixup, exec
>   * example of squashing 3 commits:
>     * before squash:
>       ```
>       a -- b -- c -- d                <-- main
>             \
>              e -- f -- g -- h         <-- branch
>       ```
>     * after squash:
>       ```
>       a -- b -- c -- d                <-- main
>             \
>              e -- i                   <-- branch
>       ```
>       * commit 'i' now contains all the changes that were in originally 'f', 'g', and 'h' -- you can keep 'f's 
         > original 
>         commit message or give it a new one
> * `git rebase -i $(git merge-base head main)` - finds the merge-base (common ancestor -> last commit before the 
   > branch) 
>   between the head (on the branch) and main and then it does an interactive rebase back to that commit
>   * example:
>     * before rebase:
>       ```
>       a -- b -- c -- d                <-- main
>             \
>              e -- f -- g -- h         <-- branch
>       ```
>     * 'b' is the merge-base
>     * after rebase:
>       ```
>       a -- b -- c -- d                <-- main
>             \
>              i                        <-- branch
>       ```
> * `git rebase <merge-base> --onto <branch_with_changes_i_want_before_mine>` - puts my current branch commits on top 
>   of the other branch's commits
> * `git rebase --abort` - abort the rebase

### Merge

> * `git merge <branch_name>` - basic merge (by default a fast-forward merge)
>   * example:
>     * before merge:
>       ```
>       a -- b -- c -- d                <-- main
>             \
>              e -- f                   <-- branch
>       ```
>     * after merge:
>       ```
>       a -- b -- c -- d                <-- main
>             \       /
>              e -- f                   <-- branch
>       ```
> * `git merge <branch_name> --no-ff` - a no fast-forward merge -- it creates a merge commit (only holds metadata 
>   not the actual changes)
>   * if there are already more commits after where the branch branched off, then git *must* do a true merge not a 
>     --no-ff merge (unless the branch is rebased first)
>   * example:
>     * before merge:
>       ```
>       a -- b                          <-- main
>             \
>              c -- d -- e              <-- branch
>       ```
>     * after merge:
>       ```
>       a -- b ----------- f            <-- main
>             \           /
>              c -- d -- e              <-- branch
>       ```
>     * f is the merge commit
>   * **NOTE**: rebasing blows away any merge commits that happened after you branched off
> * `git merge --squash`
>   * loses the history of the branch
>     * example:
>       * before merge:
>         ```
>         a -- b                        <-- main
>               \
>                c -- d -- e            <-- branch
>         ```
>       * after merge:
>         ```
>         a -- b ----------- f          <-- main
>               \ 
>                c -- d -- e            <-- branch
>         ```

### Diff

> * `git diff` - shows changes between the working tree and the index or a tree, changes between the index and a tree,
>   changes between two trees, changes between two blob objects, or changes between two files on disk
>   * this much easier to follow in the IDE
> * `git diff --name-status` - show only the names and status of the changed files

### Push

> * `git push` - pushes changes to GitHub/GitLab
>     * `origin` is the default push location if no location is provided
> * `git push --force` (`-f`)- push amended/rewritten history, overwrites a remote branch with your local branch
> * `git push --force-with-lease` (`--force-w`) - push amended/rewritten history, a safer option than --force that will 
>   not 
>   overwrite 
>   any work on the remote branch if more commits were added to the remote branch (by another team-member or coworker 
>   or what have you)
>   * will fail with a 'stale info' message if there are more commits on the remote
> * `git push -u` - makes a new remote tracking branch
> * `git push --set-upstream` - does the same thing as `git push -u`
> * `git push origin --delete <branch_name>` - deletes remote branch

### Remote

> * `git remote -v` - shows a list of the current local remote tracking branches

### Clean

> * `git clean -df` - gets rid of unstaged files that have never been committed
> * `git clean -dfx` - also cleans gitignored files

### Reset

> * `git reset --hard <commit_sha>` - Resets the index and working tree. Any changes to tracked files in the 
>   working tree since the specified commit are discarded [reset docs](https://git-scm.com/docs/git-reset)

## Helpful Links

* [GitLab git docs](https://docs.gitlab.com/ee/topics/git/)
* [Interactive git examples](https://onlywei.github.io/explain-git-with-d3/)
* [GitHub cheat sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf)
* [GitLab cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
* [Interactive visual cheat sheet](https://ndpsoftware.com/git-cheatsheet.html)
* NOTE: see [command_line notes](/self-taught/notes/command_line.md) to find info on VIM
