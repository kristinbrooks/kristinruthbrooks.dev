# jQuery

[jQuery](https://jquery.com/)
* it's a DOM manipulation library
    * a javascript library is code someone else wrote that we can use in our code
    * helps us do things faster and makes it easier to do those things

## Should You Use jQuery?

* there is a debate in the community about whether and/or when you should use jQuery
    * example of one of many websites addressing its use:
    [You might not need jQuery](http://youmightnotneedjquery.com/) 

### Why to Use it?

* fixes 'broken' DOM API
* brevity (more concise) and clarity
* ease of use
* cross-browser support
* AJAX
* lots of people use it
    * strong community, lots of resources, etc.

### Why NOT to Use it?

* DOM API is no longer 'broken' (easier to use)
* it doesn't do anything you can't do on your own
* it's an unnecessary dependency
    * if you are only using a couple of methods, you still have to include everything
* performance
    * if not familiar with source code you can end up writing code that is more inefficient than
     doing it without jQuery
* lots of people are moving away from jQuery

### Colt says...

* either way, it's worth knowing...but don't want to be dependent on it (still want to know
 vanilla JS)
 
### Chad says...

* no one uses it anymore...should still learn it because will probably run into it, but don't use
 on anything new

## Including jQuery

* download jQuery and link to it locally:
    `<script type="text/javascript" src="jquery.js"></script>`
* Link to a CDN (content distributed network --- i.e. a hosted copy):
    ```html
    <script
      src="https://code.jquery.com/jquery-3.5.1.js"
      integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
      crossorigin="anonymous">
    </script>
    ```
     
## Selecting

* can select anything with `$('selector')`
    * similar to `.querySelectorAll()` --- we provide a CSS style selector and jQuery will return
     all the matching elements
* examples:
    * all img tags - `$('img')`
    * all elements with class 'sale' - `$('.sale')`
    * all elements with id 'bonus - `$('#bonus')`
    * all a tags inside lis - `$('li a')`
    
## Common Methods

* don't have to write loops to change all selected elements like with vanilla JS, these methods
 have implicit loops
* `.css(property, value)`
    * example - give an element with id 'special' a border: `$('#special').css('border', '2px solid red');`
    * can define an object and pass that in
        * ```javascript
          let styles = {
            color: 'red',
            background: 'pink',
            border: '2px solid purple'
          };
          $('h1').css(styles);
          ```
          * all the passed styles are applied --- would have taken 3 lines in vanilla JS
    * can style multiple elements at once --- same format, just pick an element tag that has more
     than one element --- `$('li').css('color', 'yellow')` changes all the lis to yellow
* `.val()`
    * works on anything with a value --- checkbox, text input, dropdown, etc.
    * get value from first matched element
    * change value with argument
* `.text()`
    * like `.textContent()` 
    * get text or change text
        * if get text from multiple elements, it combines them in one string with no spaces
        * can change text of multiple elements at once separated by commas
    * html safe --- if enter html it will just treat it like text and not code --- stops users
     from entering code into inputs and messing up our page
* `.attr()`
    * gets the value of the first element's attribute in a set of matched attributes
        * attributes are the values included in the tags
    * sets value/s of attributes with an argument/s
    * can pass objects
* `.html()`
    * retrieves inner html
    * with an argument it changes the inner html
* `.addClass()`
* `.removeClass()`
* `.toggleClass()`
        