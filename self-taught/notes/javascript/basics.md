# Javascript Basics

* `//` for comments
* **NOTE:**DRY stands for 'Don't Repeat Yourself'
    * if not DRY, can be called WET -- 'Write Everything Twice'

## What is JavaScript/

* lightweight, cross-platform, object-oriented computer programming language
    * lightweight - uses little memory, relatively simple syntax
* one of the three core technologies of web development along with html and css
* nowadays can be used in different places:
    * client-side: JavaScript was traditionally only used in the browser
    * server-side: with Node.js, we can use JavaScript on the server as well
* JS is what made modern web development possible:
    * dynamic effects and interactivity
    * modern web applications that we can interact with
* Frameworks/libraries like React and Angular are 100% based on JS    

## Relationship between HTML, CSS, and JavaScript

* `html` is like the nouns --- `<p></p>` means 'paragraph'
    * responsible for the content of the page --- text, images, buttons, etc
* `css` is the adjectives --- `p { color: red; }` means 'the paragraph text is red'
    * responsible for the presentation of the content --- styling
* `javascript` is the verbs --- `p.hide():` means 'hide the paragraph'
    * the real programming language that allows us to allow dynamics

## JavaScript developer console

* 'Right Click -> Inspect -> Console tab' or 'View -> Developer -> Javascript Console'
* enter lines start with `>`, returned lines start with `<･` --- also have separator lines
* can up arrow to previous inputs like in terminal
* `clear ()` will clear the console

## JavaScript in a Separate File

* need an `.html` file, with at least a basic html template in it
* need a `.js` file
* need a script tag to connect the two files
    * `<script src="fileName.js"></script>`

## JavaScript Primitive Data Types

**NOTE**: for info on Primitives vs. Objects look [here](behind_the_scenes/primitives_vs_objects.md)

* JS has **dynamic typing** --- i.e. data types are automatically assigned to variables, don't have to declare the type
    * it is the value stored in a variable that has a type, not the variable itself => can reassign a variable with a
     value of a different type

### Numbers

* javascript doesn't care if a number is whole, fractional, or negative --- it doesn't differentiate
    * whether you see it or not, all numbers are floating point
* modulo is `%`

### Strings

* sequence of characters, used for text
* must be in quotes
    * single or double
* concatenation uses `+`
* escape characters with `\`
* `.length` gives us the string's length property
* access individual characters using `[]` (i.e. `"hello"[4]`)
    * javascript is 0 indexed

### Booleans

* logical data type that can only be `true` or `false`

### Null and Undefined

* in a sense they both mean 'nothingness', but in different ways

#### Undefined

* the data type of a variable that does have a value yet 
* occurs when a variable was declared but not initialized

#### Null

* special value which represents 'nothing', 'empty', or 'value unknown' (javascript.info/types)
* 'non-existent' (Jonas)
* 'explicitly nothing' (Colt)

### Symbol (ES2015)

* value that is unique and cannot be changed

### BigInt (ES2020)

* larger integers than the Number type can hold

## Variables and their Syntax

* variable store containers that store information
* javascript variables use camelCase and can only start with a letter, underscore, or dollar sign
* can't use reserved keywords as variable names
* from 1997-2015 `var` was the only way to make a variable --- now there are also `let` and `const`
* `var`
    * scoped to "current execution context" --- a variable's enclosing function, or the global
     scope --- it is not a block scoped variable
        * meaning if a variable is in a loop within a function, the variable is scoped to the
         whole function, not just the loop
    * can be reassigned whenever
    * initializing with value is optional
    * can be declared at any point
    * global variables are added to window
* `let`
    * block scoped
    * does not create value on global window object
    * initializing with value is optional
    * can be reassigned whenever
    * cannot be redeclared (in the same scope)
    * let is basically the `var` keyword but block scoped
* `const`
    * block scoped
    * cannot be reassigned
        * not immutable, but variable reference cannot change
            * can't just reassign it to something else, but if it was an array for example, you
             could edit the array or push onto it, etc.
    * must be initialized with a value
    * does not create value on global window object
    * cannot be redeclared (in the same scope)
* declaration syntax: `var/let/const yourVariableName = yourValue;`
* try to use: (according to Colt Steele; seconded by Jonas Schmedtmann):
    * `const` over `let`
    * `let` over `var`
    * use `var` pretty much never (you probably don't need it)

## Useful Built-in Methods

* `alert(whatYouWantDisplayedInTheAlert)` --- makes a popup that displays the alert
* `prompt("theTextPromptForTheUser")` --- gets input from user in a popup
* `console.log(somethingToPrint)` --- prints the input to the console
* `typeof thingWantType Of` --- 

## Strings and Template Literals

* use **backticks** -- type string -- any variables/expressions are in the format `${variableName}`/`${expression}` (no
 `+` concatenation is needed)
* backticks can also be used for any regular string --- then you don't have to worry about escaping quotes --- and if
 you need to insert variables later it is easier
* type multi line strings in backticks with the text on multiple lines instead of using newline escapes 

## Boolean Logic

* javascript has the standard comparison operators (`>`, `>=`, `<`, `<=`, `==`, `!=`) 
 plus: `===`, `!==` 
    
### Equality Operators

* `==` performs **type coercion**, while `===` does not
* example:
  ```javascript
  let x = 99;
  x == "99"     // true --- type coersion --- says string 99 and number 99 are the same
  x === "99"    // false --- strict equality --- string 99 and number 99 are not the same because
                // they are different types
  
  let y = null;
  y == undefined    // true
  y === undefined   // false
  ```
* `===` much safer, should always use if possible --- best practice
* `!=` & `!==` are the same idea as `==` & `===` except the negation

## Logical Operators

* javascript uses `&&` (AND), `||` (OR), and `!` (NOT)
* return Booleans

## Truthy and Falsy

* every value in javascript is inherently 'truthy' or 'falsy'
* a falsy value is one that is considered false in an if/else statement --- `0`, `""`, `null`,
 `undefined`, `NaN` (not a number)
* truthy value are all values that are not falsy
* example:
  ```javascript
  let height;
  
  if (height) {
    console.log('Variable is defined.');
  } else {
    console.log('Variable is not defined.');
  }
  
  // "Variable is not defined."
  //  we get this because undefined is falsy, so it is defined as false in the if/else block
  
  height = 0;
  
  // height is now defined and can be 0, but 0 is a falsy value. we get:
  // "Variable is not defined." 
  // so we need to change our statement so this doesn't happen
  if (height  || height === 0) {
    console.log('Variable is defined.');
  } else {
    console.log('Variable is not defined.');
  }
  
  // now we get "Variable is defined."
  ```
* can check by putting a value in a boolean context
    * example:
      ```javascript
      !"hello"    // false => "hello" is truthy
      !!"hello"   // true  => truthy
      ```
* example:
  ```javascript
  let str = ""                  // falsy
  let msg = "haha!"             // truthy
  let isFunny = "false"         // truthy because it is a string not a boolean
  
  !((str || msg) && isFunny)    // false
  ```
## Statements vs. Expressions

* expressions are pieces of code that always produce a value --- length doesn't matter as long as
 they produce a single value at the end
* statements are code that does something but doesn't produce any immediate value
    * ex: function declarations, if statements, etc.

## Strict Mode

* `'use strict'` --- must be the first statement in the script file
    * forbids us to do certain things
    * shows some errors in console where JS would just fail silently before
