# Advanced jQuery

## Events

* will add to all the elements if don't specify
* can't use `this` to refer to the element with the listener --- must use `$(this)` becasue must
 be running on a jQuery object

### '.click()

* select and use `.click(callBackFunction)` to add a click listener

### Keypress

* select and use `.keypress(callBackFunction)` to add a keypress listener
    * fires midway between `.keydown` and `.keyup`
    * this indicates which character was entered
* `.keydown()` fires when press key and `.keyup()` fires when release key
    * these provide a code indicating which key is pressed
    * examples: 
        * 'a' will be reported as 65 by these methods, while it will report 97 from a keypress
        * 'A' will be reported as 65 by all three methods
        * that makes these better choices when catching special keystrokes such as arrow keys
* if look at the event, 'which' will tell you which key was pressed

### '.on()'

* select and use `.on(typeOfListener, callBackFunction)` to add an on listener
* very similar to vanilla JS's `.addEventListener()` because you specify what kind of event
* Why USe `.on()`?
    * `.click()` only adds listeners for existing elements
    * `.on('click')` will add listeners for all potential future elements
        * must add the listener to an element that already exists and then add an extra argument
         that is the type of element you will be adding
            * example: $('ul').on('click', 'li', function() {...`
                * add to `li`s of the existing parent `ul`, then when add new `li`s to the `ul
                ` they will get listeners as well
    
## Effects

* see 'jqueryEffects.js'/'jqueryEffects.html' for examples of the following methods

### Fading

* `.fadeOut(optionalDuration, optionalCallBackFunction)`
* `.fadeIn(optionalDuration, optionalCallBackFunction)`
* `.fadeToggle(optionalDuration, optionalCallBackFunction)`

### Sliding

* `.slideDown(optionalDuration, optionalCallBackFunction)`
* `.slideUp(optionalDuration, optionalCallBackFunction)`
* `.slideToggle(optionalDuration, optionalCallBackFunction)`
