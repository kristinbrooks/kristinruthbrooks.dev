**NOTE**: All images from Jonas
Schmedtmann's [The Complete Javascript Course 2020](https://www.udemy.com/course/the-complete-javascript-course/)

# Overview of JavaScript

![High Level](images/bts1.png)

![Garbage Collected](images/bts2.png)

![Interpreted or Just-in-time Compiled](images/bts3.png)

![Multi-paradigm](images/bts4.png)

![Prototype-based Object-oriented](images/bts5.png)

![First Class Functions](images/bts6.png)

![Dynamic](images/bts7.png)

![Single-threaded, Non-Blocking Event Loop](images/bts8.png)
