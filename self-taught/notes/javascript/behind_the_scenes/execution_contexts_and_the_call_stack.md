**NOTE**: All images from Jonas
Schmedtmann's [The Complete Javascript Course 2020](https://www.udemy.com/course/the-complete-javascript-course/)

# Execution Context and the Call Stack

## Execution Context

![Execution Context](images/context1.png)

![Execution Context](images/context2.png)

[Execution Context: Variable Environment](variable_environment.md)

[Execution Context: Scope and the Scope Chain](scope_chain.md)

[Execution Context: The `this` Keyword](this_keyword.md)

## The Call Stack

* individual execution contexts are added one at a time to the call stack

![Call Stack](images/stack1.png)

![Call Stack](images/stack2.png)

* as each context executes, it pops off the stack until we are left in just the global context

![Call Stack](images/stack3.png)
