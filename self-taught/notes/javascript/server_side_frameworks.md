# Server Side Frameworks

## What is a Framework? How is it Different From a Library?

* a library or package is a collection of code, written by someone else, that we can include in our
 applications and use
* frameworks are also code written by other people that we can use, but usually much bigger, and
 the way we use them is very different
* the most important, and in fact the defining difference between a library and a framework is
 **Inversion of Control** (description from this [stack overflow post](http://www.stackoverflow.com/questions/3057526/framework-vs-toolkit-vs-library))
    * in a library, you are in control (you make the calls)
    * with a framework, the framework calls you (control is inverted)
        * all the control flow is already in the framework --- there's just a bunch of predefined
         white spots that you can fill out with your code
* frameworks take all the common stuff we do in every application (all the setup work, etc) and
 prepackages it up so we can use it and get started on new apps without having to do all the
  basic groundwork every time --- focus on the things that make the new app unique vs all the setup
* apps have lots of files and moving parts, the framework is the glue that keeps them together    
    
## Types of Frameworks

* for video games
* for mobile apps
* tons for making web apps
* frameworks for different languages:
    * Sinatra, Rails for Ruby
    * Django, Flask for Python
    * Sails.js for Node.js
* lightweight and heavyweight frameworks
    * refers to how much a framework does for you vs how much you have to do yourself
    * heavyweight - does a lot, you don't need to fill in as much with your own code
        * can make apps fast because it does so much for you
        * more problematic for beginners because they don't really understand what it's doing and
         can become more reliant and unable to work with lighter frameworks
        * example: Rails
    * lightweight - expected to fill in your own code more often
        * doesn't hide things from you
        * doesn't do things you don't expect it to do
        * have to understand how things work to get things done
* 'un-opinionated' - flexible, lets you do things in the way that you want (vs Rails, which is
 very opinionated)

## Express 

* Node Web Development Framework (used for Web Development Bootcamp class)
    * it is a lightweight framework
* see 'workspace/the-web-development-bootcamp/11_express/firstExpressApp' for a basic app with
 notes added
* install with `npm install express --save`


## Routes

**NOTE**: more notes on routing [here](../apis_routes.md)

### Get Requests

* the basic format:
    ```javascript
    app.get('/', function (req, res) {   // params: (url, callback function(request, response){})
      // req and res are objects
      // req contains all the info about the request that was made, that triggered this route
      // res contains all the information we're going to respond with
      res.send('Hi There!');  // whatever you want the response to be
    });
    ```
* tell the server to listen for requests (start server)
    ```javascript
    app.listen(3000, function(){    // app.listen(port, hostname, backlog, callback)
      console.log('Server has started on port 3000!!!');
    });

    ``` 
  
### Splat `*` Route Matcher

* if try to go to a route that doesn't exist, get 'Cannot GET /nonExistentRouteName'
* the `*` is used as a catch-all for any routes that don't exist
* it has a response that will be provided when someone attempts to access a non-existent route
    * can show a message or an error page, etc.

### Discuss Route Order

* it matters!
* the first route that matches a given request will be the only one that runs
* therefore,the catch-all must come **last** --- if it is first, it will match/catch everything
 and no other routes will run
  
### Routes Containing Route Parameters

* generally don't actually write a separate for every page
* the code would not be DRY and there would need to be thousands of routes
* instead, patterns are written
* Reddit as an example of why route parameters are necessary:
    * subreddits are of the form `.../r/subredditName);` but there are thousands of subreddits 
    * going further they have all the pages of the form`.../r/subredditName/comments/uniqueID/postTitle`
    and all these comments pages don't each have their own individual routes written out
        * aside from how many there are --- the posts are written by users and no one can
         anticipate all of those different posts
    * so they are matched using **route parameters**/**route variables**/**path variables** (have
     different names in different frameworks)
    * use `app.get('/r/:subredditName, callbackFunction);`
* using the colon tells the server to not actually match character for character what is there
, but to make it a pattern where any word can be after the preceding `/`
* not like a wildcard --- it won't match anything that comes afterwards if another `/` is added
 --- that is another pattern
    * example continued: it would need to be something like `app.get('/r/:subredditName/comments/:id
    /:title/, callbackFunction);`
* at this point it doesn't care about what is in any of those areas --- can be letters, numbers
, special characters, etc. --- it will match anything that is there
* to access the value in a pattern matching area so it can be used, the request object is used
    * it has all the information about the incoming request
    * example continued:
        ```javascript
        app.get('/r/:subredditName/comments/:id/:title/, function(req, res) {
          console.log(req);
          res.send('WELCOME TO THE COMMENTS PAGE!');
        });
        ```
        * look in the info that prints in the console for `params: { subredditName
        : theActualName, id: theUniqueName, title: titleOfThePost }`
* to use the params info --- example continued:
    ```javascript
    app.get('/r/:subredditName/comments/:id/:title/, function(req, res) {
      let subreddit = req.params.subredditName;
      let title = req.params.title;
      res.send('WELCOME TO THE COMMENTS PAGE FOR THE POST "' + title.toUpperCase() + '"' + ' IN' +
        'THE ' + subreddit.toUpperCase() + ' SUBBREDDIT!');
    });
    ```
### Post Requests

* look for examples in repo: `the-web-developer-bootcamp/11_express/post_request_demo`
* need to install `body-parser` package so express can take info from the html body, parses it into
 a javascript object, and use it on the server side
     