# Vue

* Vue is:
    * declarative
    * legible
    * easy to maintain
    * simple but powerful
    * a collection of the best of the best from a bunch of different frameworks
    * gives you what you want when you need it, then gets out of the way
    * a way to be really productive
    * fun!
    * (keeps us from yak shaving)
  
* [Vue Style Guide](https://v3.vuejs.org/style-guide/)

## Intro

### Vue Instance Example
  ```html
  <div id="app">{{ message }} Nice to meet Vue.</div>
  ```
  * mounting to `div id="app"` and passing in a message
  * using an id because you only want ine instance
  ```javascript
  const App = {
    data() {
      return {
        message: 'Hello World!'
    }  
   }
  }
  
  Vue.createApp(App).mount('#app')
  ```
  * the message from above is getting piped in

### Vanilla JS vs Vue Example

* **Vanilla JS**:
  ```javascript
  const items = [
    'thingie',
    'another thingie',
    'lots of stuff',
    'yadda yadda'
  ];
  
  function listOfStuff() {
  const full_list = items.map(el => `<li> ${el} </li>`).join('');
  
  const contain = document.querySelector('#container');
  contain.innerHTML = `<ul> ${full_list} </ul>`;
  }
  
  listOfStuff();
  ```
  ```html
  <div id="container"></div>
  ```
* **Vue**:
  ```javascript
  const App = {
    data() {
      return {
        items: [
          'thingie',
          'another thingie',
          'lots of stuff',
          'yadda yadda'
        ] 
      }
    }
  }
  
  Vue.createApp(App).mount("#app");

  ```
  ```html
  <div id="app">
    <ul>
      <li v-for="item in items">
        {{ item }}
      </li>
    </ul>
  </div>
  ```
  * yields:
    * legible, declarative code
    * we're holding the state of these items (in `data`), rather than the DOM holding the state,which gives us
      better control
    * clean
    * semantic html
    * easy to maintain
    * reactive (this will be explained in detail in a later section)
  