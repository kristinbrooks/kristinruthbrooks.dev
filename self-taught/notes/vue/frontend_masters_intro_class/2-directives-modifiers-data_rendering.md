# Directives, Modifiers, and Data Rendering

NOTE: the exercise from this section is [here](https://codepen.io/ktown/pen/QWKvqjB).

* *directives* are the `v-` attributes in the html

![Directives Quick Summary Chart](images/directives-chart.png)

## V-MODEL

* creates a relationship between the data in the instance/component and a form input, so you can dynamically update
  values
    * called 'two way binding'
* **Accepting user input and managing it responsibly**:
  ```javascript
  const App = {
    data() {
      return {
        message: 'This is a good place to type things.'  
    }  
   }
  }
  
  Vue.createApp(App).mount("#app");
  ```
  ```html
  <div>
    <h3>Type here:</h3>
    <textarea v-model="message" class="message" rows="5" maxlength="72"/>
    <br>
    <p class="booktext">{{ message }}</p>
  </div>
  ```
    * the `v-model="message"` is establishing a relationship between what we're holding in  `data` and what we're
      gathering from the user in the `textarea`, then we output it to the page in the `{{}}` brackets

  Output:

  ![Displays textarea directly on book illustration as typing](images/example1.png)
    * displays the text instantaneously on the book as typing
* **Checkboxes, Radio Buttons, etc**:
    * checkbox example:
      ```javascript
      const App = {
        data() {
          return {
            checkedNames: [] 
        }  
       }
      }
      
      Vue.createApp(App).mount("#app");
      ```
      ```html
      <div id="app">
        <input v-model="checkedNames" type="checkbox" id="john" value="John"/>
        <label for="john">John</label>
    
        <input v-model="checkedNames" type="checkbox" id="paul" value="Paul"/>
        <label for="paul">Paul</label>
    
        <input v-model="checkedNames" type="checkbox" id="george" value="George"/>
        <label for="george">George</label>
    
        <input v-model="checkedNames" type="checkbox" id="ringo" value="Ringo"/>
        <label for="ringo">Ringo</label>
      
        <p>checkedNames: {{ checkedNames }}</p>
      </div>
      ```
        * add a `v-model` for every input we want to add to the array

      Output:

      ![Displayed checkbox example](images/example2.png)
* Modifiers:
    * `v-model.trim` will strip any leading or trailing whitespace from the bound string
    * `v-model.number` changes strings to number inputs
    * `v-model.lazy` won't populate the content automatically --- it will wait to bind until an event happens (It
      listens to change events instead of input)
        * often used with form validation --- want to wait until a user is done filling out something before they get an
          error

## V-IF / V-SHOW

* is a conditional that will display info depending on meeting a requirement --- this can be anything - buttons, forms, divs, components --- really flexible
* `v-if` example:
  ```javascript
  const App = {
    data() {
      return {
        tacos: ''
      };
    }
  };

  Vue.createApp(App).mount("#app");
  ```
  ```html
  <div id="app">
    <h3>What is your favorite kind of taco?</h3>
    <textarea v-model="tacos"></textarea>
    <br>
    <button v-if="tacos" type="submit">Let us Know!</button>
  </div>
  ```
    * the `v-if` makes it so the button only shows if `tacos` in not an empty string --- shows up when start typing
        * we can see the `v-if` in the DOM when `tacos` is empty
          ![v-if Visible in DOM](images/example3.png)
        * when we start typing the button will show up
          ![v-if Visible in DOM](images/example3-2.png)
* `v-show` example (same code, just with `v-show` replacing `v-if`)
    * this still functions the same on the page for the user, but in the DOM you can now see the `<button>` even when
      `tacos` is empty --- it just shows with `style="display: none;"`
      ![v-if Visible in DOM](images/example4.png)

### when to use `v-if` vs `v-show`

* if you have a lot of information/a big component and you don't just want it hanging out in the DOM then it makes sense to use `v-if`
* if you have something you know is going to be toggled back and forth a lot, using `v-show` can be useful because
  mounting a whole component to the DOM can be [expensive](https://adhithiravi.medium.com/react-virtual-dom-explained-in-simple-english-fc2d0b277bc5) ([opposing view about the expense](https://svelte.dev/blog/virtual-dom-is-pure-overhead)) and here we're
  just showing and hiding repeatedly

## V-IF / V-ELSE / V-ELSE-IF

* you can conditionally render one thing or another
* they must be siblings (not nested)
* example:
  ```html
  <div id="app">
    <h3>Do you like tacos?</h3>
  
    <input v-model="tacos" value="yes" type="radio" id="yes">
    <label for="yes"> yes</label>
    <br>

    <input v-model="tacos" value="no" type="radio" id="no">
    <label for="no"> no</label>
    <br>
  
    <p v-if="tacos === 'yes'" class="thumbs">👍</p>
    <p v-else-if="tacos === 'no'">you're a monster</p>
  </div>
  ```

## V-BIND or :

* there is a shortcut (`:`) because this is one of the most useful directives
* used for many things --- class and style binding, creating dynamic props, etc...
* class binding example:
    ```javascript
  const App = {
    data() {
      return {
        tacos: '',
        activeClass: 'active'
      };
    }
  };

  Vue.createApp(App).mount("#app");
  ```
  ```html
  <div id="app">
    <h3>What is your favorite kind of taco?</h3>
    <textarea v-model="tacos"></textarea>
    <br>
    <button :class="[tacos ? activeClass : '']">Let us Know!</button>
  </div>
  ```
  ```scss
  button {
    border: none;
    color: white;
    padding: 0.5em 1em;
    border-radius: 3px;
  }
  
  button.active {
    background: orangered;
  }
  ```
    * the ternary adds or removes a class based on whether `tacos` is

      empty: ![tacos is empty](images/example5.png)

      or

      not empty: ![tacos is not empty](images/example5-2.png)
* interactive style bindings: [full code example](https://codepen.io/sdras/pen/jwwpap)
  ```html
  <div id="contain" :style="{ perspectiveOrigin: `${x}% ${y}%` }">
    ...a bunch of other code...
  </div> 
  ```
    * this binds x and y coordinates to this style on mousemove, so the graphic moves as we move the mouse

      <img src="images/example6.png" width="250">  
      <img src="images/example6-2.png" width="250">

## V-FOR

* used above in the 'Vanilla JS vs Vue Example' section (`v-for="item in items"`)
* it loops through a set of values
* can also do a static number:
  ```javascript
  Vue.createApp({}).mount("#app");
  ```
  ```html
  <div id="app">
    <ul>
      <li v-for="num in 5" :key="num">
        {{ num }}
      </li>
    </ul>
  </div>
  ```
  ![static list of numbers](images/example7.png)
* object example:
  ```javascript
  const App = {
    data() {
      return {
        jokes: {
          question: 'What did one baby prototype say to the other?',
          answer: `I'll race you to class!`,
          response: 'groan'
        }
      }
    }
  }
  ```
  ```html
  <div id="app">
    <p v-for="(value, key, index) in jokes" :key="value">
      {{ index }}. {{ key }}: {{ value }}
    </p>
  </div>
  ```
  ![object example](images/example8.png)
    * always have access to `(value, key, index)` even if you don't name them that --- and they will always be
      referenced in that order in the `v-for`
      ![object example](images/example8-2.png)

### KEY

* `:key` is what Vue uses to efficiently track VDOM changes/diffs
* needs to be unique
    * Sarah recommends using a library like [uuid](https://www.npmjs.com/package/uuid) that will generate unique ids
      --- if were to get coffee shop names as inputs for example, give each input its own id...then if there are
      multiple Starbucks, they will each have a unique id to tell them apart even though their names are the same
* example from above: `<li v-for="num in 5" :key="num">` - using num to track which one it is --- 1 then 2 then 3...
* needs to be numbers or string

## V-ONCE and V-PRE

* `v-once` will not update once it's been rendered
* `v-pre` will print out the inner text exactly how it is written, including code (good for documentation)
* example:
  ```html
  <div id="app">
    <h3>What is your favorite kind of taco?</h3>
    <p><input v-model="tacos" /></p>
    <p v-once="tacos">{{ tacos }}</p>
    <span v-pre>This is good if I need to show the mustache view of {{ tacos }}</span>
    <pre>{{ $data }}</pre>
  </div>
  ```
  ```javascript
    const App = {
    data() {
      return {
        jokes: {
          tacos: 'I like Al Pastor tacos'
        }
      }
    }
  }
  
  Vue.createApp(App).mount('#app')
  ```
    * initial result:
      ![initial result](images/example9.png)
        * the `v-pre` kept the formatting of `{{ tacos }}` when and prints it as a string rather than evaluating it
    * result after changing the input:
      ![result post changed input](images/example9-2.png)
        * the `v-once` line remained unchanged after its initial rendering
        * the `<pre>` tag is a standard html tag that is outputting pretty printed code to the page
            * the `$` in Vue is often referring to the Vue instance itself --- in this case it will print whatever is in
              `data` in the JS code to the page

## V-ON or @

* for binding events like click and mouseenter --- you can pass in a parameter for the event like (e)
* can use ternaries directly
* example:
  ```html
  <div id="app">
    <div class="item">
      <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/28963/backpack.jpg" width="235" height="300"/>
      <div class="quantity">
        <button class="inc" @click="counter > 0 ? counter -= 1 : 0">-</button>
        <span class="quant-text">Quantity: {{ counter }}</span>
        <button class="inc" @click="counter += 1">+</button>
      </div>
      <button class="submit" @click="">Submit</button>
    </div><!--item-->
  </div>
  ```
  ```javascript
  const App = {
    data() {
      return {
        counter: 0
      }
    }
  }

  Vue.createApp(App).mount('#app')
  ```
    * using the `@click` events in the quantity div to increment the number being purchased --- use a ternary in the
      `@click` to keep the quantity from going less than 0
* Modifiers:
    * `@mousemove.stop` is comparable to `e.stopPropigation()`
    * `@mousemove.prevent` this is like `e.preventDefault()`
    * `@submit.prevent` this will no longer reload the page on submission
    * `@click.once` (not to be confused with `v-once`) this *click event* will be triggered once
    * `@click.native` lets you listen to native events in the DOM
    * Keycodes - use names instead of numbers (starting with Vue 3)

### Multiple Bindings

* can be useful for things like games --- could bind an event to both the mouse and a key so they can choose which
  way to control the game
* example:
  ```html
  <div v-on="
    mousedown: doThis, 
    mouseup: doThat
  ">
  </div>
  ```

## V-HTML

* for strings that have html elements that need to be rendered --- will treat it like html instead of trying to
  evaluate it like it would in `{{}}` brackets
* example:
  ```javascript
  const App = {
    data() {
      return {
        tacos: `I like <a href="http://www.epicurious.com/recipes/food/views/tacos-al-pastor-242132" target="_blank">Al Pastor</a> tacos`
      }
    }
  }

  Vue.createApp(App).mount('#app')
  ```
  ```html
  <div id="app">
    <h3>What is your favorite kind of taco?</h3>
    <p v-html="tacos"></p>
  </div>
  ```
* WARNINGS:
    * not the same as templates: inserted as plain html
    * Don't use on user-rendered content, avoid XSS attacks

## V-TEXT

* similar to using mustache templates (`{{}}`) --- have both so that if you only have access to attributes and
  aren't able to insert something you can still get the same functionality
*
```html
<div id="app">
  <h3>What is your favorite kind of taco?</h3>
  <p v-text="tacos"></p>
  <p>{{ tacos }}</p>
  <p><input v-model="tacos" /></p>
</div>
```
```javascript
const App = {
  data() {
    return {
      tacos: 'I like Al Pastor tacos'
    }
  }
}

Vue.createApp(App).mount('#app')
```
* WARNING:
    * if you want to dynamically update, recommended that you use mustache template instead --- they are evaluated faster
  