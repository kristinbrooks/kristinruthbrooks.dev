# Backend Basics

## Internet Basics

1. enter the site you want to visit
2. computer needs to find IP address to send request to
    * query submitted to your ISP
    * within ISP, the DNS takes the domain name and turns it into an IP address
3. going to the address
    * request sent to IP address via HTTP (hyper text transfer protocol)
    * request finds the fastest possible path to the server with the specified IP
        * this is not a direct journey --- it requires hopping from server to server until we
         arrive  
4. server responds
    * requested server finds out exactly what we're asking for
    * server builds the right content --- often pulling info from a database
    * server responds with any combo of HTML, CSS, and Javascript
5. browser renders the page  

## Static vs Dynamic Sites

### Static

* same HTML, CSS, and Javascript every time

### Dynamic

* compiled on the server side
    * server pulls everything together and constructs it before it is sent back as a response
* can have different HTML, CSS, and/or Javascript that's being sent back every time a request is
 made

## Stacks/Backend Technologies

* a generic stack: 

     | Backend language/app | server | database |
     |----------------------|--------|----------|
     |----------------------|--------|----------|
     |        ⬇            |                   |
     |   HTML/JS/CSS        | ➡︎      | website  |
        
     Note:dashed row separates backend from frontend 
    * HTML, CSS, and Javascript are almost always part of a stack
    * much more variance in the backend
        * languages: ruby, python, scala, java, php, javascript, etc.
        * databases: mongo, postgres, mysql, etc.
* example of Reddit's stack:

     | Python/Flask         | nginx  | postgresql |
     |----------------------|--------|------------|
     |----------------------|--------|------------|
     |        ⬇            |                     |
     |HTML/JS/CSS (Backbone)| ➡︎      |Reddit site |
     
* backend is where the dynamic part of the site comes in
    * examples of possible backend features:
    
     |check if the user is logged in  |sign up user        |remove post from DB  |
     |--------------------------------|--------------------|---------------------|
     |figure out what js/html/css to send the user | add new post to DB |   sort/rank posts   |
     |                                | create new comment |   create subreddit  |
     |                                |                    | add to newsletter   |
     |--------------------------------|--------------------|---------------------|
     |                ⬇              |                                          |
     |           HTML/JS/CSS          |         ➡︎          |    website          |

* stack for Web Developer Bootcamp (Udemy class):
  
     |   Node JS   | Express |   MongoDB    |
     |-------------|---------|--------------|
     |-------------|-------- |--------------|
     |     ⬇      |                        |
     | HTML/JS/CSS |    ➡︎    | project site |
* example of going to a website and then searching for 'dogs':

     | Frontend | | Backend |
     |----------|---|---------|
     |1. ask for homepage| | |
     | | |2. send back home page content|
     |3. browser renders home page| | |
     |4. user enters 'dogs' in the search and submits form| | |
     | | |5. finds all posts in DB about 'dogs'|
     | | |6. sends back HTML for the search results page|
     |7. browser renders search page| | |
