# Advice On How to Become a Developer

* **Note**: images are from section 5 of [The Complete Javascript Course](https://www.udemy.com/course/the-complete-javascript-course/)

![How to Succeed at Learning To Code 1](images/advice1.png)
![How to Succeed at Learning To Code 2](images/advice2.png)
![How to Succeed at Learning To Code 3](images/advice3.png)
![How to Succeed at Learning To Code 4](images/advice4.png)

![Trajectory to Getting Your First Developer Job](images/advice5.png)

![How to Fil at Solving Problems](images/advice6.png)
![Solving Problems: step 1](images/advice7.png)
![Solving Problems: step 2](images/advice8.png)
![Solving Problems: step 3](images/advice9.png)
![Solving Problems: step 4](images/advice10.png)