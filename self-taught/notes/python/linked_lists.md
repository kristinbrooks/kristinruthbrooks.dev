# Python linked lists

## linked lists

* behind the scenes, Python uses an array to create a list
    * their elements are stored in sequentially numbered memory locations
    * to insert an element requires all following elements to shift down so still in sequential memory order
    * this makes arrays not efficient when things need to be added and removed frequently
* for large collections of data, linked lists are much more efficient
* a linked list is a collection of data that can be stored as nodes in non-consecutive memory locations
    * each node carries a pointer to the next element in the list
        * the last element typically points to null
* to insert an element, nothing needs to be shifted, you just need to adjust the pointers
* disadvantages:
    * each node must carry a pointer to the next node, which requires resources, so they take up more space than
    array lists
    * does not allow random access
        * example:
          if you want to find the 3rd item in the list, you must begin at the head of the list and follow each pointer
          until you find it
* types:
    * singularly linked list - each node points to the next node
        * can only step thru in one direction
    * circular linked list - last element points back to the beginning of the list
    * doubly linked list - each element contains two pointers, one to the next node and one to the previous node
    
## creating a linked list

1. define a node class
  ```
  class Node:
    def __init__(self, data = None, next = None):
        self.data = data
        self.next = next

    def print_node(self):       # not required, but makes it easier to print
        print(self.data)
  ```
2. define linked list class
  ```
  class LinkedList:
    def __init__(self):
        self.head = None    # in the future this will hold the head of the linked list
                            # but initially you will have a new, empty linked list
    def append_node(self, data):
        if not self.head:
            self.head = Node(data)
            return
        else:
            current = self.head
            while current.next:     # continues until there is no next node
                current = current.next
            current.next = Node(data)       # then assigns data to the next node

    def print_list(self):       # not required, just makes it easier to print
        node = self.head
        while node is not None:
            print(node.data)
            node = node.next
  ```

## searching a linked list

* can perform a linear search
* if reach `node.next == None`, then reached the end of the list without finding the target
* example:
    ```
    def search( self, target):
        current = self.head
        while current != None:
            if current.data == target:
                print('Found it!')
                return True
            else:
                current = current.next
        print('Not found.')
        return False
    ```
