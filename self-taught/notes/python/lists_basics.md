# Python Lists

* a kind of container along with tuples, and dictionaries

## general format and indexes
* format is `list_name = [element_0, element_1, element2]`
* can mix types on a single list
* can have empty list `list = []`
* reference an element by `list_name[element_index]`
* to get length of list `len(list_name)`
* 0 indexed
* when using negative indexes -1 is the last item on the list
  * think length of 'list - negative_index = positive_index'; ex: if you have a list whose length is 5 and you want
  index -3, then 'len(list) - 3 = 2', +2 is the same index as -3
* trying to access things outside index range gives index error
* `list.index(element)` -  returns the index of the first occurrence of the element
  * error if element isn't on list, so check first

## adding elements

* `list.insert(index, element)` - -inserts element at that index and shifts all the following elements down
  * note: if you assign an element with `list[index] = element` to an index that already has an element in it
  the existing element is overwritten rather than inserted
* add elements to list with `list_name.append(new_element)` and the new element will be added to the end of the list
* `list.extend(other_list)` - all the elements on `other_list` are now at the end of `list`
* note: `.append` adds single element, `.extend` merges a list onto another
  * example:
    ```
    >>> list1 = [1, 2, 3]
    >>> list = [4, 5]
    >>> list1.append(list2)
    >>> list1
    [1, 2, 3, [4, 5]]
    ```
    ```
    >>> list1 = [1, 2, 3]
    >>> list = [4, 5]
    >>> list1.extend(list2)
    >>> list1
    [1, 2, 3, 4, 5]
    ```
* `list1 + list2` - works like `extend` but creates a new list and leaves the original lists unchanged
* `list1 += list2` - works just like `extend`

## removing elements

* remove elements from list with `list_name.pop()` removes the last element
  * use `list.pop(index)` to get an element that isn't the last one on the list
  * it returns the popped element
* `list.remove(element)` - removes the element
  * if the element occurs multiple times in the list it removes only the first occurrence
  * get error if try to remove an element that isn't on the list, so good to check that it is in the list first
* note: `.pop` takes an index, `.remove` takes an element

## checking if element in list

* to see if a list contains an element use `in` and it will evaluate to a Boolean, so it can be used anywhere you
can use a Boolean test --- assignment, loops, conditionals
  * example:
    ```
    x = 1
    if x in list_name:
        # do something
    ```
    
## misc.
    
* `list.copy()` - returns copy of the list
* `max(list)`, `min(list)`, `sum(list)`  - returns the min value, max value, or sum of the elements in the list

## iterating through list

* standard for loop:
  ```
  for i in range(len(list)):
    # do something
  ```
* for each loop:
  ```
  for elem in collection:
    # do something with element
  ```
* both for loops iterate over all elements of the list in order

## loops

* ```
  for i in range(len(list)):
    list[i] += 1    # modifying list in place
  ```
  * use 'for loop with range' when modifying primitive type elements in a list
  * you can always use this one
* ```
  for elem in list  # modifying local variable elem; if elem is primitive type, not changing the list!!!
    elem += 1
  ```
  * use 'for-each loop' when not modifying elements or when the elements are not primitive types

## passing as parameters

* lists are passed by reference
* in the function, value changes in list persist after the function ends
* creating a new list in a function means you're not dealing with the list passed in as parameter
  * it's like the URL you are using is pointing to a new page and you are no longer changing the parameter list
  * example:
    ```
    def create_new_list(num_list):
        num_list.append(9)
        num_list = [1, 2, 3]
    
    def main():
        values = [5, 6, 7, 8]
        create_new_list(values)
        print(values)
    ```
    Output is `[5, 6, 7, 8, 9]`
    