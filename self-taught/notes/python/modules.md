# Python Modules

* module is another name for a Python file with code in it
* can use code from one module in another module
* there are also built in modules
  * examples: math, keyword, statistics, etc.
* to use a module you must `import` it, then you can use its functionality
  * imports should be at the top of file
  * use `name_of_module.method(params)
