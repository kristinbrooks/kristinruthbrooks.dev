# Python lists extended

## combining two lists

* can use Python's built-in zip function
  * it will take 0 or more iterables and return a new list of combined tuples
  * example:
    ```
    movies = ['Interstellar', 'Inception', 'The Prestige', 'The Dark Knight', 'Batman Begins']
    ratings = [1, 10, 10, 8, 6]
    new_list = []
    for tree in zip(movies, ratings)
        new_list.append(tree)
    print(new_list)
    ```
    prints `[('Interstellar', 1), ('Inception', 10), ('The Prestige', 10),
    ('The Dark Knight', 8), ('Batman Begins', 6)]`
  * only works when there are the same number of elements in both lists
    * it will only return a list where there are elements in both lists and throw away the rest
      * example:
        if the ratings list above only had the first 3 entries the zip function would have returned
        `[('Interstellar', 1), ('Inception', 10), ('The Prestige', 10)]`
        
## list comprehensions

* allows you to create lists based on criteria applied to existing lists
* general syntax: `new_list = [expression(i)] for i in input_list if filter(i)]`
    * `expression(i)] for i` is based on the variable used for each variable in the input string
        * example: `c for c` - use each item in the list
        * example: `price * 3 for price` - multiply each item in the list by 3
        * example: `word[0] for word` - use first letter of each word
    * `in input_list` specifies the input string or list
    * `if filter(i)` allows you to add a conditional statement to filter out list items that match specified criteria
        * example: `if c.isdigit()`
        * example: `if n%2 == 0`
* example to find last digit in a string:
    ```
    input_string = 'Buy 1 get 2 free'
    new_list = [c for c in input_string]  # list comp says to loop through each char in string to create a new list
    print(new_list)
    ```
    prints `['B', 'u', 'y', ' ', '1', ' ', 'g', 'e', 't', ' ', '2', ' ', 'f', 'r', 'e', 'e']`
    ```
    input_string = 'Buy 1 get 2 free'
    new_list = [c for c in input_string if c.digit()]  # adding the if clause only lets digits be added to the list
    print(new_list)
    
    ```
    prints `['1', '2']`
    ```
    input_string = 'Buy 1 get 2 free'
    new_list = [c for c in input_string if c.digit()][-1]  # tack on a negative index to select only the last digit
    print(new_list)
    
    ```
    prints `2`
    
## find the intersection of two lists

* intersection is the elements that are common to both lists
* example:
    this week's winning lottery numbers: 2, 43, 48, 62, 64, 28, 3
    most common winning numbers: 1, 28, 42, 70, 2, 10, 62, 31, 4, 14
    ```
    def return_intersection(list1, list2):
      list3 = [value for value in list1 if value in list2]
      return list3
  
    list1 = [2, 43, 48, 62, 64, 28, 3]
    list2 = [1, 28, 42, 70, 2, 10, 62, 31, 4, 14]
    print(return_intersection(list1, list2))
    ```
    prints `[2, 62, 28]`
* can also use Python's built-in `intersection()`
    * works for sets not for lists
        * can change lists into sets like `set1 = set(list1)`
    * syntax: `set1.intersection(set2)`
    * convert back into list using list function: `list(set1.intersection(set2))`
    * reworking of above example:
        ```
        def return_intersection(list1, list2):
          set1 = set(list1)
          set2 = set(list2)
          return list(set1.intersection(list2))
      
        list1 = [2, 43, 48, 62, 64, 28, 3]
        list2 = [1, 28, 42, 70, 2, 10, 62, 31, 4, 14]
        new_list = return_intersection(list1, list2)
        print(new_list)
        ```
        prints `[2, 28, 64]`
    * can work with more than 2 sets
        * `set1.intersection(set2, set3, set4)`, etc
    