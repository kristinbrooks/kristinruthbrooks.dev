# Algorithms

* (examples in Python unless otherwise noted)

* a series of steps that can be followed to solve a problem
* examples: 
    * palindrome - same forwards and backwards
        * reverse order of string, make both all lower or upper, remove any punctuation, and see if two strings match
    * FizzBuzz - print list of numbers, but sub 'Fizz' for multiples of 3, 'Buzz' for multiples of 5, and 'FizzBuzz'
    for multiples of 3 and 5
        * use modulo `%` with conditionals
    * anagram - two words with all the same letters
        * match their cases,sort both words alphabetically, then see if match 
        * Python has a built in function `sorted()` that will alphabetize for you
    * count number of times each character occurs in a string
        * ```
          def count_characters(string):
            count_dict = {}
            for c in string:
            if c in count_dict:
                count_dict[c] += 1
            else:
                count_dict[c] = 1
          print(count_dict)
          ```
          * each key will be a character and each value will be the number of times it occurred in the string

# Related Notes

* Searching
* Recursion
