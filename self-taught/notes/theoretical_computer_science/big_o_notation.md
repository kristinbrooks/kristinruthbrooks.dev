# Big O Notation

* Big O is a tool to help us compare the efficiency in terms of time and space(RAM) of 2 functionally equivalent pieces of code

Common functions for Big-O: (pic from [here](https://runestone.academy/runestone/books/published/pythonds/AlgorithmAnalysis/BigONotation.html))

![Common functions for Big-O](images/big-o-names.png)

(following pics are all from the Joy of Coding Academy Big O presentation)

Graphs of the common functions:

![Graphs of Common Functions](images/big-o-graphs.png)

![Big-O Complexity Chart](images/big-o-complexity.png)

![Picturing Efficiency](images/big-o-efficiency1.png)

![Picturing Efficiency](images/big-o-efficiency2.png)

![Picturing Efficiency](images/big-o-efficiency3.png)

![Picturing Efficiency](images/big-o-efficiency4.png)

![Picturing Efficiency](images/big-o-efficiency5.png)
