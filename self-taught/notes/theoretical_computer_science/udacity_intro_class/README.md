# Intro to Theoretical Computer Science

**NOTE**: These are notes from the [Intro to Theoretical Computer Science](https://www.udacity.
com/course/intro-to-theoretical-computer-science--cs313) course on Udacity. The classwork can be found [here]
(https://gitlab.com/kristinbrooks/theoretical-computer-science)

* will cover 2 parts of theoretical computer science: complexity theory and computability
    * complexity theory: the study of how much -- how much time and/or resources?
    * computability: asks is this possible? can a computer even solve the problem given unlimited time and resources
