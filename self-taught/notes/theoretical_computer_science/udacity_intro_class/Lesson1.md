# Lesson 1

## Challenging Problems

### Alice's Problem

* programmer at tele-communications company - experienced, but hasn't studied theoretical computer science
![Communications Network](images/alice-network.png)
image: connected communications centers -- notice that each center is not connected to all the other centers, they are 
selectively connected through the blue cables -- this is very simplified, her actual network has about 500 centers
* company needs to know that every cable is working -- they can install monitoring devices, which will check 
  integrity of all cables connected to that device
    * ![monitoring device](images/monitoring-device.png)
      image: a monitoring device and the cables it can monitor
    * each center does not need its own device because some will be covered by devices at other centers
* Alice needs to determine how many devices they need and where to place them

### Alice's Algorithm

* choosing randomly obviously won't work, so she needs an algorithm
* the algorithm that will work for sure: try all possibilities
* pseudocode:
  ```
  # initial condition
  minimum_devices = number of communication centers
  # 0-no monitoring device, 1-gets monitoring device
  for each assignment of (0,1) to the communcation centers:
    if assignment is valid      # valid means all cables are monitored
        number_of_devices = number of "1"s in assignment
        minimum_devices = min(minimum_devices, number_of_devices)
  ```
* since each center has 2 options, the number of combinations to check will be 2^(# of centers)
* therefore, with this algorithm on Alice's network of 500 centers, way more than a trillion trillion combinations would have to be tested
* this is actually a **challenging problem**

### Problems and Algorithms

* problems need: name, input, output - and to know if it's a hard or easy problem
  * if there is a simpler algorithm then it is an easy problem, if not it is a hard problem
* algorithm: description of computer program to solve a given problem

## Analyzing Algorithms

### Simplifications

1. analyze without implementing
   * RAM model
1. "worst possible" inputs
   * worst-case running time
1. ignore "unnecessary" details
   * Big-O-Notation (aka "Landau" notation)
    
#### RAM (random access machine)

* using a model as defined by Steven Skiena in "The algorithm design manual"
* RAM needs:
    1. memory
       * for intermediate results and output
       * has as many cells as the algorithm needs 
    1. input/output
        * read only
    1. programming
        * read only
* we are interested in the time the program will run for a given input
    1. "simple" (adding, if, for, etc.) operations take 1 timestep
    1. loops count for as many timesteps as the number of times they run
    1. accessing memory (assignments,etc.) is free - 0 timesteps
    * example:
        ```
        s = 5
            while s > 0: # this comparison counts as a simple operation
                s = s - 1
        ```
        * takes 11 timesteps: the loop executes 5 times and the comparison is executed 6 times ( it will still have to 
          compare when s = 0, but the loop won't execute a 6th time)
* in the real world, these it is unrealistic that simple operations always take 1 timestep, we have as much memory 
  as we need for every problem, and that memory access is free
* it is realistic that a unit of memory cannot hold an arbitrarily large number - depends on bits in each unit of memory
* while the model is unrealistic, it can still tell us somewhat realistically if certain parts of our algorithm are 
  making it run faster or slower
  
#### Running Time vs Structure / "Worst Possible" Inputs

* example:
    ```
    input: string s[0]...s[n-1]     # ie. string of length n
        
    count = 0                   # 0 timesteps
    for character in s:         # n or (n + 1) times depending on how you count the loop 
        if character == 'a':    # n times
            count = count + 1   # a times
    ```
* running time: `2n + 1a + 0` or `2n + 1a + 1`
* even in this simple example running time is based on the size of the input (length of the string, n) and the 
  structure of the input (the number of times 'a' occurs)
* as the algorithm gets more complex the running time formula gets very, very complicated -- most of the time we also don't know what kinds of strings it will encounter
* 3 ways to avoid these complications:
    1. optimistic view: best input      (no 'a's -> `2n + 1`)
    1. pessimistic: worst input         (all 'n's -> `3n + 1`)
    1. average view: 'average' input    (very hard to define an 'average' string without a very precise 
       definition of 'average')
    * so it will always run between `2n + 1` and `3n + 1` times
* we will use **worst-case running time** analysis
    * we get guarantees - it will never take longer than the worst case
* example:
    ```
    input: string s[0]...s[n-1]     # ie. string of length n
    
    count = 0
    for i in range(n - 1):
        if s[i] == 'a':
            if s[i + 1] == 'b':
                count = count + 1      
    ```
* possible inputs:
  
  |             | best case | worst case |   ???   |
  | :---------  | :-------: | :--------: | :-----: |
  | abababab... |           |    ✔️      |         |
  | aaaaaaaa... |           |    ✔️      |         |
  | acacacac... |           |            |   ✔️    |
  | bbbbbbbb... |    ✔️     |            |         |

#### Big-O-Notation

* 2n<sup>2</sup> + 23 vs 2n<sup>2</sup> +27 
    * you wouldn't really care about the 23 vs the 27, so we would say they have the same running time
* Big-O-Notation leaves out all those things we don't really care about when we compare algorithms
* | Algorithm A | Algorithm B |
  |-------------|-------------|
  | running time: | running time: |
  | 3n<sup>2</sup> - n + 10 | 2<sup>n</sup> - 50n + 256 |
  Which algorithm is preferable?
    * In general, A is preferable because 2^n grows much faster than n^2 as long as n isn't very small
    * when comparing A and B we didn't pay attention to many of the terms in the algorithms -- 
        ~~3~~n<sup>2</sup> ~~- n + 10~~, 2<sup>n</sup> ~~- 50n + 256~~
    * we decided that for some value of n B will always grow faster than A
* **Generalizing**: let f(n) and g(n) be 2 running time algorithms, then:
    1. there's some numbers n', c so that c * g(n') ≥ f(n')
    1. for all n'' > n', we have c * g(n'') ≥ f(n'')
    
       => f(n) ∈ O(g(n))
        * the O means that g(n) grows at least as fast as f(n)
        * this is where the 'Big-O' comes from
        * also sometimes written: f(n) = O(g(n)) (commonly used) or f(n) ⊂ O(g(n)) (uncommon, ⊂ means subset)
* allows us to look at just the fastest growing parts of the functions
* graphical example: ![Big-O example graphs](images/big-o.png)
* examples:
  
    3n + 1 ∈ O(n),

    3n +1 ∈ O(n<sup>2</sup>) -- but it would be unusual to write it like this because we are trying to state the tightest bound possible
  
    18n<sup>n</sup> - 50 ∈ O(n<sup>2</sup>),
  
    2<sup>n</sup> + 30n<sup>6</sup> +123 ∈ O(2<sup>n</sup>),
  
    **2<sup>n</sup> * n<sup>2</sup> + 30n<sup>6</sup> + 123 ∉ O(2<sup>n</sup>)**,
  
    2<sup>n</sup> * n<sup>2</sup> + 30n<sup>6</sup> + 123 ∈ O(2<sup>n</sup> * n<sup>2</sup>),

    50 * 2<sup>n</sup> * n<sup>2</sup> + 5n - log(n) ∈ O(2.1<sup>n</sup>) -- true, but not best possible bound
* same example as previous section:
    ```
    input: string s[0]...s[n-1]     # ie. string of length n
    
    count = 0
    for i in range(n - 1):
        if s[i] == 'a':
            if s[i + 1] == 'b':
                count = count + 1   
    
    ```
    * things to notice:
        1. considers each character at most twice (once when it is `i + 1` and once when it is `i`)
        1. for each character in the input string, a constant number of steps is performed ( if it finds an 'a' it 
           will do either 1 or 2 additional operations, if it doesn't find an 'a' it will do 0 additional operations)
         
           => c<sub>1</sub> * n + c<sub>2</sub> ∈ O(n) -- also known as a linear algorithm
* example:
  
  (hint: n + (n - 1) + (n - 2) + ... + 2 + 1 = (n<sup>2</sup> + n) / 2)
    ```
    result = 0                      # 0 timesteps
    for i in range (0, n):          # n timesteps
        for j in range (i, n):      # ((n^2 + n) / 2) timesteps 
            result = result + j     # ((n^2 + n) / 2) timesteps
    ```
    => running time ≈ 0 + n + 2((n<sup>2</sup> + n) / 2) 
  
    = n + n<sup>2</sup> + n 
  
    = n<sup>2</sup> + 2n 
  
    => O(n<sup>2</sup>)
    * running in 2 loops like this almost always results in quadratic running time

##### Landau Notation

* Edmund Landau, 1877 - 1938, didn't come up with the notation but he popularized it
* he was known as an exact and pedantic mathematician, so it's funny that he popularized an approximation notation

### Analyzing Alice's Algorithm

```
1 minimum_devices = number of communication centers
2 for each assignment of (0,1) to the communcation centers:
3   if assignment is valid       # valid means all cables are monitored
4     number_of_devices = number of "1"s in assignment
5     minimum_devices = min(minimum_devices, number_of_devices)

# line 1: O(1) -- constant time
# line 2: O(2^n)
# line 3: each time it is executed: O(n^2) -- depends on number of cables being checked, see image below
# line 4: each time it is executed: O(n)
# line 5: each time it is executed: O(1)
```
![possible number of cables](images/cables.png)

=> running time is O(2<sup>n</sup> * n<sup>2</sup>)
* explanation:
    * line 1 takes constant time so we can ignore it
    * lines 2-5 run O(2<sup>n</sup>) times 
    * lines 3-5 run a maximum of n<sup>2</sup> + n + 1 times => O(n<sup>2</sup>)
        * this loop will execute 2<sup>n</sup> times -- once for every time the 'for' loop runs
    
    => O(2<sup>n</sup> * n<sup>2</sup>)
