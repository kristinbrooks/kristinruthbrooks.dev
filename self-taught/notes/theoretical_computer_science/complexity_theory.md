# Computational Complexity Theory

* subfield of theoretical computer science
* primary goal "is to classify and compare the practical difficulty of solving problems about finite combinatorial
  objects" [source](https://plato.stanford.edu/entries/computational-complexity/)
* "Complexity helps determine the difficulty of a problem, often measured by how much time and space (memory) it 
  takes to solve a particular problem. For example, some problems can be solved in polynomial amounts of time and 
  others take exponential amounts of time, with respect to their input size." [source](https://brilliant.org/wiki/complexity-theory/#:~:text=Complexity%20theory%20is%20a%20central%20topic%20in%20theoretical%20computer%20science.&text=Complexity%20theory%20helps%20computer%20scientists,problems%20in%20its%20complexity%20class.)
* complexity theory proposes "a formal criterion for what it means for a mathematical problem to be *feasibly decidable* – i.e. that it can be solved by a conventional Turing machine in a number of steps which is 
  proportional to a polynomial function of the size of its input. The class of problems with this property is known 
  as P – or *polynomial time*... P can be formally 
  shown to be distinct from certain other classes such as EXP – or *exponential time*...complexity class known as NP 
  – or *non-deterministic polynomial time* – consisting of those problems which can be correctly decided by some 
  computation of a non-deterministic Turing machine in a number of steps which is a polynomial function of the size 
  of its input. A famous conjecture – often regarded as the most fundamental in all of theoretical computer science – states that P is also properly contained in NP – i.e. P⊊NP." [source](https://plato.stanford.edu/entries/computational-complexity/)
  
## Real World Implications

* can be important to algorithm design and analysis
* often described in big-O notation
* knowing if an algorithm runs in polynomial time vs exponential time can tell us how efficient an algorithm is

## Decision Problems

* "Such a problem corresponds to a set X in which we wish to decide membership." [source](https://plato.stanford.edu/entries/computational-complexity/#ComCom)
* "...problems that can be answered with a "yes" or a "no." For example, "is this number prime?", "does this graph 
  have a hamiltonian path?""is there an assignment of variables to the equation such that a set of constraints are 
  satisfied?"" [source](https://brilliant.org/wiki/complexity-theory/#:~:text=Complexity%20theory%20is%20a%20central%20topic%20in%20theoretical%20computer%20science.&text=Complexity%20theory%20helps%20computer%20scientists,problems%20in%20its%20complexity%20class.) 
* famous problems: [Travelling Salesperson Problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem#Benchmarks), [3SAT](https://brilliant.org/wiki/3sat/), [minimum spanning tree 
  problems](https://en.wikipedia.org/wiki/Minimum_spanning_tree#:~:text=The%20minimum%20labeling%20spanning%20tree,edge%20in%20a%20spanning%20tree.), [primality testing](https://brilliant.org/wiki/prime-testing/)
* can be simulated on computational models like Turing machines
