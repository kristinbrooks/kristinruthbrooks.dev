# Rails Basics

* the following notes are from [this tutorial](https://guides.rubyonrails.
* org/getting_started.html)

## Creating a New Application

* `$ rails new application-name` - creates a Rails app called Application-name in 
  an `application-name` directory
  * see all options accepted by the Rails application generator with `$ rails new --help`

### Contents of the New Application's Directory

* `app/` - Contains the controllers, models, views, helpers, mailers, channels, jobs, and assets for your application. You'll focus on this folder for the remainder of this guide.
* `bin/` - Contains the `rails` script that starts your app and can contain other scripts you use to set up, update, deploy, or run your application.
* `config/` - Contains configuration for your application's routes, database, and more. This is 
  covered in more detail in [Configuring Rails Applications](https://guides.rubyonrails.org/configuring.html).
* `config.ru` - Rack configuration for Rack-based servers used to start the application. For 
  more information about Rack, see the [Rack website](https://github.com/rack/rack).
* `db/` - Contains your current database schema, as well as the database migrations.
* `Gemfile`, `Gemfile.lock` - These files allow you to specify what gem dependencies are needed 
  for your Rails application. These files are used by the Bundler gem. For more information 
  about Bundler, see the [Bundler website](https://bundler.io/).
* `lib/` - Extended modules for your application.
* `log/` - 	Application log files.
* `public/` - Contains static files and compiled assets. When your app is running, this directory will be exposed as-is.
* `Rakefile` - This file locates and loads tasks that can be run from the command line. The task definitions are defined throughout the components of Rails. Rather than changing `Rakefile`, you should add your own tasks by adding files to the `lib/tasks` directory of your application.
* `README.md` - This is a brief instruction manual for your application. You should edit this file to tell others what your application does, how to set it up, and so on.
* `storage/` - Active Storage files for Disk Service. This is covered in [Active Storage 
  Overview](https://guides.rubyonrails.org/active_storage_overview.html).
* `test/` - Unit tests, fixtures, and other test apparatus. These are covered in [Testing Rails 
  Applications](https://guides.rubyonrails.org/testing.html).
* `tmp/` - Temporary files (like cache and pid files).
* `vendor/` - A place for all third-party code. In a typical Rails application this includes vendored gems.
* `.gitignore` - This file tells git which files (or patterns) it should ignore. See [GitHub - 
  Ignoring files](https://docs.github.com/en/get-started/getting-started-with-git/ignoring-files) for more info about ignoring files.
* `.ruby-version` - This file contains the default Ruby version.

## Starting the Web Server

* from the application's directory use `bin/rails server`
* starts the default web server, Puma, that comes with Rails

## Stopping the Web Server

* `ctrl-c

## Autoloading

* Rails apps **do not** use `require` to load application code
* application classes and modules are available everywhere => **should not** load anything in 
  `app` with `require`
* only need `require` in two cases:
  * to load files under the `lib` directory
  * to load gem dependencies that have `require: false` in the `Gemfile`

## Generating a Model

* a *model* is a Ruby class used to represent data
* they can interact with the app's database through a Rails feature called *Active Record*
* names are singular
  * use command `bin/rails generate model <Model name> <any colums you want to create with the 
    generator>`
    * example: `bin/rails generate model Article title:string body:text`
      * this generates the model:
        ```ruby
        class CreateArticles < ActiveRecord::Migration[7.0]
          def change
            create_table :articles do |t|
              t.string :title
              t.text :body

              t.timestamps
            end
          end
        end
        ```

## Database Migrations

* used to alter the structure of an application's database
* migration command: `bin/rails db:migrate`
* in Rails, migrations are database-agnostic

## Using Model to Interact with the Database

