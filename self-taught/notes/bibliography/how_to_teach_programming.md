# How to Teach Programming (and other things)

* about children learning to code
* interesting comparison between teaching methods in english (phonics vs. whole language), math
  (practice vs. exploration/proofs), and programming
* Seymour Papert (created LOGO, programming language for kids) studied with Jean Piaget
  (psycologist,founder of constructivism)
* Papert said "By explaining something you take away the opportunity for a child to discover
  it" --- i.e. don't explain --- common vibe in programming instruction books for kids
* in other things (especially outside of school), like guitar or tennis, you aren't expected to
  just figure it out ---  you are taught and must do deliberate practice --- practice the
  small things
* this is like the phonics side in the language teaching debate
* comparing the types of teaching
    * group 1-> instruction, phonics, explaining (direct instruction)
    * group 2-> inquiry, whole language, exploration
    * turns out explaining things to kids works really well
* it is always said based on research from the 50s that we can only hold 7 +- 2 items in our
  short term memory --- newer research indicates it may actually only be 4 +- 2
* we use chunking to remember more things (e.g. remembering words vs. all the individual
  characters)
* when our short term memory gets full we feel cognitive load
* KEY POINT-> You don't become an expert by doing expert things
* research on direct instruction of programming->
    * vocalize syntax --- reading code out loud --- group that read code aloud every week did
      better than the kids in the control group that didn't (paper --- The effect of reading
      code aloud on comprehension by Swidan and Hermans '19)
    * tell students how to solve problems --- works really well (paper --- The case for case
      studies in Pascal by Linn and Clancy '92)
        * three groups
          * write own code and then read expert code
          * write own code and then read expert code with explanation
          * read expert code with explanation
        * teachers preferred the first group method
        * the second and third groups did equally better than the first group --- lightens
          cognitive load, leaving more room to remember things
        * takeaway-> teaching explicit strategies works really well
    * assessment (specifically in code clubs) --- gives insight into what students actually
      know and helps them remember it better
* KEY POINT-> having fun is not important for learning
* there is the idea that Motivation => Skill --- they are related, but it's actually usually
  in the opposite way --- Skill => Motivation --- if acquiring skill then will be motivated
