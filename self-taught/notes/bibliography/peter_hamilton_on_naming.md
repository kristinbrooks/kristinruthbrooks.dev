# Peter Hamilton on Naming

* naming is hard even though it's not difficult in a technical way
* Hilton says it's hard because it's communication
* programming is about communicating with compuers but also other programmers who will read
  the code
* names are part of making beautiful, clean code
* takes creativity, but also a solid understanding of the thing you are naming
* one of most common refactorings
* bad name examples-> data, object, thing, foo
*good names are good in their particular context
