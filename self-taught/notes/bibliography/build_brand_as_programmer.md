# How to build your brand as a programmer

* start a blog
* build side projects
* use social media to build your brand
* write a book
* build your brand with speeches
