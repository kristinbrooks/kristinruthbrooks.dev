# How to Not Read the Room - Creating Wearables with ML

* from Strange Loop 2019
* necklace of blinking tiles made to respond to those around the wearer
* changes the colors and patterns based on preset patterns and the number of people it detects
* HARDWEAR-> raspberry pi, arduino, camera, LEDs, battery
* camera is around neck, the other hardware is strapped around the waist
* SOFTWARE-> NodeJS, Tensorflow.js, C++
* had previously made things that responded to other people --- one responded to what she
* was saying, another responded to input from an app
* necklace was to force her to be social --- it was crazy and obnoxious when she wasn't
* around people, but if she was around people it would turn off
