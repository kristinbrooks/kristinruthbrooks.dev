# AstriaGraph - Monitoring Global Traffic in Space

* from Strange Loop 2019
* don't have any space traffic rules, but do have space highways
* currently track about 26,000 objects
* only about 2,500 are functional --- everything else is garbage
* 10,000-15,000 satellites planned to be launched in the next 5 years or so
* really need worldwide transparency about location and event data
  * need independent monitoring, best processes and standards
    * for example --> India blew up one of its own satellites which created tons of debris
      * they claim it will all come down on one time frame but others think they're wrong
* need tracking vs just detecting
* comprehensive space object recognition is unsolved
* different groups/bodies (owner, government, etc) can't agree on locations of objects --- they
  make different assumptions that effect the physics
* AstriaGraph is a mapping system for the space objects --- can manipulate 3 dimensionally
  and zoom in and out
  * when zoom in you can see the potential different locations of objects reported by the
    different groups/bodies
* should determine 'space footprints' for objects to determine their safety and effect on
  other objects and the environment
  * then we would be able to monetize collecting space garbage by placing bounties or similar
  * also it would help insurance companies underwriting satellites
* speaker would like to introduce crowdsourcing independent sources of info, if can find a way
  to link the sources in information together --- this could help look for discrepancies in info
* amateur astronomers generally like to try and track what they think are hidden/spy satellites
* UN has non-legally binding guidelines put together by committees from different countries ---
  it would be really helpful if countries would take these and pass them into law
