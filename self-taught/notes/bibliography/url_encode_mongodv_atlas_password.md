# How to Url Encode Your MongoDB Atlas Connection Password

* if you have special characters in password, replace them with the corresponding encodings (Hex
  value)with a `%` added to the beginning
  * example --> `p@ssw0rd'9'!` becomes `p%40ssw0rd%279%27%21`
* [ASCII Codes Table](https://ascii.cl/)
* [Encoder](https://www.urlencoder.org/)
