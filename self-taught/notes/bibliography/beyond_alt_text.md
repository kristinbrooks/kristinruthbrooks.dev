# Beyond Alt-Text - Trends in Online Accessibility

* from Strange Loop 2019
* SECTION 1 - TERMINOLOGY
  * disability-> a physical, mental, cognitive, or developmental condition that limits a
      person's ability to engage in certain tasks or participate in typical daily activities
  * assistive technology-> tools that help people perform tasks that might otherwise prove
  * challenging or impossible --- canes, hearing aids, siri, wheelchairs, screen readers,
      screen magnification, adaptive keyboards
  * a11y-> accessibility - a***********y (11*s for 11 letters between a and y)
  * Americans with Disabilities Act (ADA) 1990, amended in 2009
  * Web Content Accessibility Guidelines (WCAG 2.1)-> defines how to make the Web more
      accessible to people with disabilities - A (lowest); AA (mid-tier); AAA (highest)
  * Accessible Rich Internet Applications (ARIA)-> HTML attributes that define ways to make
      web content more accessible
* SECTION 2 - WHY WE DO THIS (A HISTORY LESSON)
  * in physical spaces, there are a lot of affordances which exist to help people with
      disabilities 
  * Strong Disability Movement in 60s and 70s --- still not perfect (obviously)
  * The Curb-Cut Effect-> the idea that improvements we make for users with disabilities end up
      making things better for everyone
  * now there is a push for disability rights in our digital spaces --- there is legislation and
      guidelines --- the software companies just need to put emphasis on it and make it happen
* SECTION 3 - ACCESSIBILITY GREATEST HITS
  * making sure websites work with keyboards
      *  can tab through a page
      *  'blue outline' to show where focus is (default feature ina ll web browsers)
      *  adjust focus when things like overlays pop up
      *  skip links
  * Alt Text and Screen Reader Text
      *  screen reader users can understand what the active element is
      *  context needs to be properly communicated to user
          *  informative images, icons, etc should have alt text descriptions
          *  decorative things should have empty string alt text, which will remove them from
            the tab order since they are necessary
  * Headings
      *  screen readers rely on HTML headings to understand how the content on the page
        is organized (think of this as like a table of contents for the page)
      *  headings on the same level don't need to be styled the same
  * Low Vision
      *  magnification (this a curb-cut effect feature --- people zoom in for all
        kinds of reasons)
      *  general contrast
      *  high contrast themes
  * Cognitive Accessibility
      *  users have varied reading levels and cognitive ability
          *  if they can't understand the sentences in a FAQ post it is likely an accessibility
          issues (for the content writers)
          *  if they can't underrated how to use the site it might be an accessibility issue(for
          the designers)
              *  or sometimes it's that the user doesn't know how to use their assistive
              technology
* SECTION 4 - EVERYBODY NEED TRAINING
  * Google's free online course
      *  "Web Accessibility By Google - Developing with Empathy"
          *  on Udacity
  * Frontend Masters
      *  Accessibility in Javascript Applications by Marcy Sutton
  * SECTION 5 - TEST FOR A11Y & SHARE KNOWLEDGE
  * there are companies to help with this (if can afford them) because it is hard to tell if
      thing will work for everyone
  * can have an a11y dashboard for tracking a11y metrics
      *  good for very simple things, but not big issues
  * can get input from local a11y communities, meet-ups, etc
  * make your knowledge available to others
      *  document it --- coding best practices, design best practices, etc
      *  can have an accessibility Slack channel
