# Machine Learning Failures - for Art!

* from Strange Loop 2018
* [speaker's blog](https://www.aiweirdness.com/)
* about interesting ways in which AIs fail at their intended tasks
* main takeaway - the failures are often surreal and more interesting than if it had worked
  as intended (as long as it's not for something really important - like medical diagnosis, etc)
* talk sections:
  * Failure 1 -- Limited info
  * Failure 2 -- Don't look too closely
  * Failure 3 -- Limited memory
  * Failure 4 -- It was not prepared for this
  * Failure 5 -- Problem is too broad
  * Failure 6 -- Training data is all-important
* some projects discussed:
  * cookiebot that got into Harvard dorms
  * generated pick-up lines, jokes, April fools prank ideas
  * making up new colors, cookie types, ice cream types, recipes
  * deciding if a name given is a pony or a metal band
  * photo identification/generation -- sheep that aren't there, giraffes everywhere, creepy
    people with the wrong number of orfices, etc
    * one of sites used as an example -- http://demo.visualdialog.org/
  * [AI art collaboration](https://aiartists.org/tom-white#:~:text=Tom%20White%20is%20a%20New%20Zealand%20based%20artist%20investigating%20artificial,of%20Wellington%20School%20of%20Design)
  * SkyKnit -- [AI generated knitting patterns](https://aiweirdness.com/post/173096796277/skyknit-when-knitters-teamed-up-with-a-neural) -- lots of tentacles, human debugging, and
    hurting brains trying to figure them out
    * [article about project](https://www.theatlantic.com/technology/archive/2018/03/the-making-of-skyknit-an-ai-yarn/554894/)
