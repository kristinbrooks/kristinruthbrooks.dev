# Intro Info

## UX vs UI vs Product Designers?

* if you're independent or working at a small place, you're pretty much all of these at the same
 time
* these terms are often used really loosely
 
### UI

* person responsible for making things
    * visual themes, fonts, colors, consistent icons, etc. --- things you see on the page
* don't really have the testing phase
* more like traditional design --- get it done and get it out there
   
### UX

* all the things from UI, plus you're looking at the brief and the users and building for that
 specifically and testing it
* user is the focus
* stay within guidelines given for budget or by the production designer, etc.

 
### Product Designer

* generally more responsibility
* on a team, more managerial => only at larger companies
* work with clients, engineers, UI designers, UX designers etc.
* has a global sense of the project from all the different teams and making sure it all works
 together --- timeline, budget, clients happy, etc

## UX Brief and persona

* have to make sure get the brief right --- often clients haven't done much UX and don't know
 exactly what they need
* brief sections:
    * Header
    * Project Name
    * Project Description
    * Who is this for?
        * build out a persona for the users
    * Feature List (Product Requirements)
        * the unique stuff, not things like 'About' section, 'Homepage', etc.
    * Competitors & Product Inspiration
    * Deliverables
        * wireframes - basic mockup for client approval
        * high fidelity - has all the colors, fonts, images, etc. ---looks just like the final
         project
        * user testing
        * usability report --- make any necessary changes based on this info
        * UI assets for developers --- images, code, icons, symbols, etc for them to build the
         site/app
        * Not Included --- just so it's clear for the clients what isn't included
    * Cost
        * set price for what's covered in the deliverables, and an hourly rate for anything
         additional they request
    * Timeline/Deadline
        * update as you go and things get pushed back so the client knows what the current
         timeline is through all stages
* user personas --- made after some research
    * give a name, age, job, and location
    * description
        * details of user's life
        * things about them that are relevant to the project and/or type of person they are
        * what the user wants/needs from the product
         