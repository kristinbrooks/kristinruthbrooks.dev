# Keyboard Shortcuts

* `spacebar` + mouse-click and drag - cursor changes to a hand, and you can move the artboard around
* `cmd +` or `cmd -` - zoom in/out
* `cmd 0` - zooms all the way out so can see all artboards
* `cmd 1` - makes size 100%
* `cmd 3` - zooms to selection
* `cmd shift '` - turn the layout on and off
* `cmd l` - lock an object
* `option 8` - insert a dot bullet point
 