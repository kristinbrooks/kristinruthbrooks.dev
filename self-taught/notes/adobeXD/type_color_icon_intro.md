# Type, Color, and Icon Introduction

## Type in wireframes

* Point Text Box (one click) - goes on forever - for Headings and such
    * when selected, can change size or rotate the text with the white dot at the bottom
* Area Text Box (click and drag) - has set width - for body, paragraphs, etc
* edit text in boxes with text options on the right toolbar

## Basic colors and buttons

* don't do a lot with color in wireframes --- keep a very basic pallet
* use `+` in color picker to save your pallet colors

## Icons

* always want SVG (scalable vector graphic)
* places to find icons:
    * adobe stock
        * just search for what you want, can't limit to only icons
    * material.io    
        * click resources -> icons
        * also just a generally good resource for UI design --- has articles, etc.
    * iconfinder.com
    * font awesome
