# Curriculum Meeting Notes

## Things to Bring up at Next Meeting

* move Flanderizer
* start working on redoing thewoolleyweb.com?

## High level goals

* get everything about website the way I want
  * automatically capturing all data about study in the right formats

## Questions/Thoughts

* If you are going to be doing a lot of work on the website up front, you want to make sure you pick technologies you want to stick with long-term, and also ones that are going to give a lot of learning value and look impressive to potential employers.
* I think those are:
  * GitLab CI + Netlify CD
  * Jamstack
  * Hugo (deployment, templating, data, etc)
  * Javascript in a functional programming paradigm
  * Typescript
  * Testing and TDD


## Skills needed for website

### Getting basic site up

* Hugo
* GitLab CI + Netlify CD deployment of Hugo

### Wiring in data to drive dynamic pages

(ordered:)
* Learn testing (with Jest in javascript)
* Learn regular expressions
* Learn functional javascript
* Learn Ramda
* Learn Typescript
* Learn how Hugo displays data
* Write the first dynamic data-driven part of site, with functional/typescript/ramda

## TODOs

* Focus:
  * Projects: Website, or sandbox projects to learn tech for website
  * Classes: Learning tech for website
  * Readings: Theory
    * Pragmatic programmer
    * Grokking Algorithms
    * One of functional programming that is simple and accessible.
* Forget due dates, but still track start/end dates
* Split Curriculum Plan doc into two separate docs: "Plan", and "Ideas/Options" (can pick better names).  Each one has high-level grouping by Project/Classes/Reading
  *  "Plan": Stuff you are definitely planning on working on over the next N months, ordered in priority.
    * More detailed for closer things, less detailed for further out.
    * Link to external plans where appropriate - e.g. to Website issue boards and/or issues.
  * "Ideas/Options": List of stuff you may do eventually, so you don't forget it.  Move these to "Plan" doc as you progress.
* Plan out high level issues for website, in order on issue board
  * Can make them hierarchical via features.  "Feature issues" vs "Implementation Issues" (latter can just be MRs if you don't need to see them on the board)
  * Can have name of Feature Issue as part of Implementation Issue or MR for organization. E.g. "Hugo Site", and "Hugo Site: Runs Local", "Hugo Site: Deployment"
* Split website into smaller projects
  * Pick first data driven part of site to work on (transcript, etc)
  