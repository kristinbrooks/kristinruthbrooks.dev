# Self-Taught Study Plan

## **CURRENT**

### *Classes*

[comment]: <> (* Introduction to Vue 3 &#40;Frontend Masters&#41;)
* The Complete JavaScript Course ([Udemy](https://www.udemy.com/course/the-complete-javascript-course/))
* Advanced CSS and Sass ([Udemy](https://www.udemy.com/course/advanced-css-and-sass/))
* (The Web Developer Bootcamp 2020 ([Udemy](https://www.udemy.com/course/the-web-developer-bootcamp/)) updated weeks 
  after I completed it so I'm checking out the changes. Original version was made in 2015, so among other things, all the ES6 info is new.)
* Into to Theoretical Computer Science ([Udacity](https://www.udacity.com/course/intro-to-theoretical-computer-science--cs313))  

### *Projects*

#### kristinruthbrooks.dev

* do Nuxt blog tutorial
* get markdown files up on site

#### cosMaker.place / cosMaker.space

* plan site layout
* decide which technology will be used
* decide if backend will be in JS or another language?

### *Reading*

* The Pragmatic Programmer
* Real World Functional Programming

## **NEXT/UPCOMING**

### *Classes*

### *Projects*

### *Reading*

* Grokking Algorithms
* The Nearly Free University & The Emerging Economy (maybe just the intro)

## **Completed** 

NOTE: this list contains things that were completed starting from 2020-12-21 when this document was created --- things completed previous to this date can be found in the activities log

#### kristinruthbrooks.dev

#### cosMaker.place / cosMaker.space
