# Ideas for Future Studies, Projects, Fun Things, etc.

## Events

## Classes

* [Purchased Udemy Classes](https://www.udemy.com/home/my-courses/learning/)
* Introduction to Machine Learning - NEW 2020 ([MIT OpenCourseware](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-036-introduction-to-machine-learning-fall-2020/index.htm?utm_source=OCWDept&utm_medium=CarouselSm&utm_campaign=FeaturedCourse))  
* Analysis of Algorithms ([Princeton via Coursera](https://www.coursera.org/learn/analysis-of-algorithms))
* Computer Science: Algorithms, Theory, and Machines ([Princeton via Coursera](https://www.coursera.org/learn/cs-algorithms-theory-machines?edocomorp=free-courses-high-school&ranMID=40328&ranEAID=EHFxW6yx8Uo&ranSiteID=EHFxW6yx8Uo-omYHAHhsW1C3B0YX7DSBJw&siteID=EHFxW6yx8Uo-omYHAHhsW1C3B0YX7DSBJw&utm_content=10&utm_medium=partners&utm_source=linkshare&utm_campaign=EHFxW6yx8Uo))
* Computer Science: Programming with a Purpose ([Princeton via Coursera](https://www.coursera.org/learn/cs-programming-java?edocomorp=free-courses-high-school&ranMID=40328&ranEAID=EHFxW6yx8Uo&ranSiteID=EHFxW6yx8Uo-mingesMolQE5zEBjGHGtiw&siteID=EHFxW6yx8Uo-mingesMolQE5zEBjGHGtiw&utm_content=10&utm_medium=partners&utm_source=linkshare&utm_campaign=EHFxW6yx8Uo))  
* Algorithms, Part 1 ([Princeton via Coursera](https://www.coursera.org/learn/algorithms-part1?edocomorp=free-courses-high-school&ranMID=40328&ranEAID=EHFxW6yx8Uo&ranSiteID=EHFxW6yx8Uo-sgIvn0o07in5SQfayESafA&siteID=EHFxW6yx8Uo-sgIvn0o07in5SQfayESafA&utm_content=10&utm_medium=partners&utm_source=linkshare&utm_campaign=EHFxW6yx8Uo))
* Algorithms, Part 2 ([Princeton via Coursera](https://www.coursera.org/learn/algorithms-part2?edocomorp=free-courses-high-school&ranMID=40328&ranEAID=EHFxW6yx8Uo&ranSiteID=EHFxW6yx8Uo-4q9qdRm4odSVaaQ6wKc6Aw&siteID=EHFxW6yx8Uo-4q9qdRm4odSVaaQ6wKc6Aw&utm_content=10&utm_medium=partners&utm_source=linkshare&utm_campaign=EHFxW6yx8Uo))
* Mathematics for Computer Science ([MIT OpenCourseware](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-fall-2010/))  
* [Analysis of Algorithms](https://www.youtube.com/watch?v=A2bFN3MyNDA&list=PLOtl7M3yp-DX32N0fVIyvn7ipWKNGmwpp)
* [Open Source Society University - Computer Science](https://github.com/ossu/computer-science)
* [freeCodeCamp](https://www.freecodecamp.org/learn/)
* [The Odin Project](https://www.theodinproject.com/paths)
* Artificial Intelligence ([MIT OpenCourseware](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-034-artificial-intelligence-fall-2010/index.htm?utm_source=OCWDept&utm_medium=CarouselSm&utm_campaign=FeaturedCourse))
* [List of all MIT OpenCourseware Classes](https://ocw.mit.edu/courses/find-by-topic/#cat=engineering&subcat=computerscience&spec=softwaredesignandengineering)
* [Become a Full Stack Web Developer](https://github.com/bmorelli25/Become-A-Full-Stack-Web-Developer/blob/master/README.md)

## Books

* Intro to Nearly Free University
* Grokking Algorithms (have physical)  
* Learn Python 3 the Hard Way & Learn More Python 3 the Hard Way  
* Clean Code: A Handbook of Agile Software Craftsmanship  
* Computer Systems: A Programmer’s Perspective (have digital)
* Haskell Programming from first principles (have digital)
* Learn You a Haskell for Great Good! (have physical)
* Introduction to Functional Programming-Bird/Wadler (have digital)
* Category Theory for Programmers (have physical/digital)
* Introduction to Functional Programming-Harrison (have digital)

## Challenges

* [#100DaysOfCode](https://www.100daysofcode.com/)
* [Cyclic Programming Challenges](https://dev.to/asabeneh/cyclic-programming-challenges-21nh)

## Video Lectures

* [Strange Loop Archive](https://www.thestrangeloop.com/)

## Podcasts

* [CodeNewbie](https://www.codenewbie.org/)

## Projects

* Web scrape for themed quotes
* Quote Generator  
* Fractal Generator ([examples](https://www.flickr.com/photos/botond-balazs/))

## Open Source Project Resources

* [6 starting points for open source beginners](https://opensource.com/life/16/1/6-beginner-open-source)

## Project Idea Websites

* [Rosetta Code Programming Tasks](http://rosettacode.org/wiki/Category:Programming_Tasks)
* [Programming Projects for Advanced Beginners](https://robertheatoncom/2018/12/08/programming-projects-for-advanced-beginners/)
* [10 Great Programming Projects to Improve Your Resume and Learn to Program](https://levelup.gitconnected.com/10-great-programming-projects-to-improve-your-resume-and-learn-to-program-74b14d3e9e16)
* [25 Javascript Project Ideas](https://www.codeconquest.com/blog/25-javascript-project-ideas/)
* [20+ Projects You Can Do with JavaScript](https://skillcrush.com/blog/projects-you-can-do-with-javascript/)
* [JS Code Challenges](https://www.codewars.com/collections/js-code-challenges)
