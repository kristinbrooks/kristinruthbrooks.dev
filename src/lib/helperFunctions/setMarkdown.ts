export default async (slug: string) => {
  const markdownPromisesByPath = import.meta.glob('$lib/../../self-taught/notes/**/*.md', {as: 'raw'})
  const markdownBySlug = {}
  for (const markdownFilePath in markdownPromisesByPath) {
    await markdownPromisesByPath[markdownFilePath]().then((markdown) => {
      const slugPath = markdownFilePath.slice(19)
      console.log(slugPath)
      markdownBySlug[slugPath] = markdown
    })
  }
  console.log(slug)
  return {
    markdown: markdownBySlug[slug],
  }
}
