const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
]

export default (date: string) => {
  if (date === '') {
    return '-'
  }
  const parts = date.split('-')
  const monthIndex: number = parseInt(parts[1]) - 1
  return `${months[monthIndex]} ${parts[2]}, ${parts[0]}`
}
