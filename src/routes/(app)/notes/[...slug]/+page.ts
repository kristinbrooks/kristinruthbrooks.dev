import type {Load} from '@sveltejs/kit'
import setMarkdown from '../../../../lib/helperFunctions/setMarkdown'


// noinspection JSUnusedGlobalSymbols
/** @type {import('./$types').PageLoad} */
export const load: Load = async ({params}) => {
  return await setMarkdown(params.slug)
}

